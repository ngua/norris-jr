{
  inputs = {
    nixpkgs.url = "github:NixOS/nixpkgs/nixpkgs-unstable";
    flake-parts.url = "github:hercules-ci/flake-parts";
    treefmt.url = "github:numtide/treefmt-nix";
  };

  outputs = { self, nixpkgs, flake-parts, treefmt, ... }@inputs:
    flake-parts.lib.mkFlake { inherit inputs; } {
      systems = nixpkgs.lib.systems.flakeExposed;
      imports = [
        flake-parts.flakeModules.easyOverlay
        treefmt.flakeModule
      ];
      perSystem = { config, pkgs, lib, system, ... }:
        let
          mkNorris =
            { returnShellEnv ? false
            , compiler ? "ghc924"
            , ...
            }:
            let
              p = pkgs.haskell.packages.${compiler}.developPackage {
                inherit returnShellEnv;
                root = self;
              };
            in
            p.overrideAttrs (_: { passthru = { inherit compiler; }; });
        in
        rec {
          devShells.default =
            let
              norris = mkNorris { returnShellEnv = true; };
              # E.g `ghc924` -> `92`
              compilerForHls = lib.concatStrings (
                lib.take 2 (
                  lib.drop 3 (lib.stringToCharacters norris.passthru.compiler)
                )
              );
            in
            norris.overrideAttrs (
              old: {
                buildInputs = with pkgs; builtins.concatLists [
                  old.buildInputs
                  (builtins.attrValues config.treefmt.build.programs)
                  [
                    (
                      haskell-language-server.override {
                        supportedGhcVersions = [ compilerForHls ];
                      }
                    )
                    cabal-install
                    hlint
                    config.treefmt.build.wrapper

                    # TODO
                    # Set these in `postBuild` in `mkNorris`
                    ueberzug
                    w3m
                    xdg_utils
                    file
                    poppler_utils
                    jp2a
                    imagemagick
                  ]
                ];
                # TODO
                # Same here
                shellHook = ''
                  XDG_DATA_DIRS=/home/$USER/.nix-profile/share:/etc/profiles/per-user/$USER/share
                  XDG_DATA_DIRS=$XDG_DATA_DIRS:/nix/var/nix/profiles/default/share:/run/current-system/sw/share
                  export XDG_DATA_DIRS
                '';
              }
            );

          formatter = treefmt.lib.mkWrapper pkgs treefmt.config;

          treefmt.config = {
            projectRootFile = "flake.nix";
            programs = {
              nixpkgs-fmt.enable = true;
              ormolu = {
                enable = true;
                package = pkgs.haskellPackages.fourmolu;
                ghcOpts = [ "TypeApplications" ];
              };
            };
          };
        };
    };
}
