{ compiler ? "ghc884", sources ? import ./nix/sources.nix }:

let
  pkgs = import sources.nixpkgs {};

  hls = import sources.all-hls {
    platform = "Linux";
    version = "0.9.0";
    ghc = "8.8.4";
  };

  gitignore = pkgs.nix-gitignore.gitignoreSourcePure [ ./.gitignore ];

  myHaskellPackages = pkgs.haskell.packages.${compiler}.override {
    overrides = self: super: {
      "norris" = self.callCabal2nix "norris" (gitignore ./.) {};
    };
  };

  shell = myHaskellPackages.shellFor {
    packages = p: [ p."norris" ];
    shellHook = ''
      XDG_DATA_DIRS=/home/$USER/.nix-profile/share:/etc/profiles/per-user/$USER/share
      XDG_DATA_DIRS=$XDG_DATA_DIRS:/nix/var/nix/profiles/default/share:/run/current-system/sw/share
      export XDG_DATA_DIRS
    '';
    buildInputs = with pkgs.haskellPackages; [
      cabal-install
      hlint
      floskell
      implicit-hie
      cabal-fmt
      hls
      pkgs.ueberzug
      pkgs.w3m
      pkgs.xdg_utils
      pkgs.file
      pkgs.poppler_utils
      pkgs.jp2a
      pkgs.imagemagick
    ];
    withHoogle = true;
  };

  exe = pkgs.haskell.lib.justStaticExecutables (myHaskellPackages."norris");

in
{
  inherit shell;
  inherit exe;
  "norris" = myHaskellPackages."norris";
}
