-- |
module Norris ( runApp, app ) where

import           Brick               ( App(App) )
import qualified Brick               as B
import qualified Brick.BChan         as B

import           Control.Applicative ( (<**>) )
import           Control.Concurrent  ( forkIO, threadDelay )
import           Control.Monad       ( forever, void )

import qualified Graphics.Vty        as Vty

import           Lens.Micro

import           Norris.App
                 ( AppState
                 , applicationName
                 , newAppEnv
                 , updateAppState
                 )
import           Norris.Types        ( Opts(Opts) )
import           Norris.UI
                 ( Event(WatchFs)
                 , Name
                 , appStartEvent
                 , drawUI
                 , handleAppEvent
                 )

import           Options.Applicative ( Parser )
import qualified Options.Applicative as O

runApp :: IO ()
runApp = do
    eventChan <- B.newBChan 20
    void . forkIO . forever
        $ B.writeBChan eventChan WatchFs >> threadDelay 2000000

    initVty <- buildVty
    os <- O.execParser opts
    env <- newAppEnv eventChan os 100
    st <- updateAppState env

    void $ B.customMain initVty buildVty (Just eventChan) app st
  where
    buildVty = Vty.standardIOConfig >>= Vty.mkVty

    opts     = O.info (options <**> O.helper)
                      (O.fullDesc <> O.progDesc "The norris-jr file manager")

app :: App AppState Event Name
app = App
    { B.appDraw         = drawUI
    , B.appChooseCursor = B.showFirstCursor
    , B.appHandleEvent  = handleAppEvent
    , B.appStartEvent   = appStartEvent
    , B.appAttrMap      = (^. #config . #attrMap)
    }

options :: Parser Opts
options = Opts
    <$> (O.optional . O.strOption
         $ O.long "config"
         <> O.short 'c'
         <> O.metavar "PATH"
         <> O.help (mconcat [ "Path to configuration file, "
                            , " default $XDG_CONFIG_HOME/"
                            , applicationName
                            , "/norris.conf"
                            ]))
    <*> (O.optional . O.strOption
         $ O.long "cache"
         <> O.short 'C'
         <> O.metavar "PATH"
         <> O.help "Cache directory, default $XDG_CACHE_HOME")
    <*> (O.optional . O.strOption
         $ O.long "working-dir"
         <> O.short 'w'
         <> O.metavar "PATH"
         <> O.help "Initial working dir")
    <*> (O.switch
         $ O.long "clean" <> O.help "Don't write to data or cache dirs")
