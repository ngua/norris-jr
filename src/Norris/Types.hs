{-# LANGUAGE AllowAmbiguousTypes #-}
{-# LANGUAGE TypeFamilyDependencies #-}
{-# LANGUAGE ViewPatterns #-}

-- |
module Norris.Types
    ( AppEnv(..)
    , Name(..)
    , Error(..)
    , pattern EvChar
    , Settings(..)
    , defaultSettings
    , Config(..)
    , Command(..)
    , Event(..)
    , AbsPath(AbsPath)
    , IsFilePath(..)
    , KeyEvent
    , BindingHints
    , BindingMode(..)
    , Pending(..)
    , ModeHints
    , Undoable(..)
    , AppState(..)
    , Entry(..)
    , Entries
    , Item(..)
    , MetaData(..)
    , FileType(..)
    , Browser(..)
    , History(..)
    , Recent
    , Username
    , Groupname
    , SavedPositions
    , NameCache
    , BrowserCache
    , Owner(..)
    , MimeType(..)
    , FileManager(..)
    , DirTree(..)
    , Child(..)
    , Extension(..)
    , MimeMap
    , Executable(..)
    , Preview(..)
    , FInfo(..)
    , Console(..)
    , Input(..)
    , Scrollback(..)
    , InputMode(..)
    , Scrollable(..)
    , SearchText(..)
    , CommandText(..)
    , UserPrompt(..)
    , Direction(..)
    , SettingsCommand(..)
    , FileOpCommand(..)
    , ControlCommand(..)
    , Position(..)
    , NavCommand(..)
    , Destination(..)
    , SearchResults(..)
    , YankOptions(..)
    , PutOptions(..)
    , DeleteOptions(..)
    , Yanked(..)
    , Renamed(..)
    , Opener(..)
    , Popup(..)
    , VisualCommand(..)
    , SortOrder(..)
    , Tag(..)
    , Tags
    , Bookmark(..)
    , Bookmarks
    , HighlightStyle(..)
    , Provider(Provider)
    , ClipboardSupport(..)
    , UserInfo(..)
    , XdgDirs(XdgDirs)
    , ShellCmd(..)
    , ProcessFlag(..)
    , FallbackApplications(..)
    , HardDeps
    , TabCompletion(..)
    , TabCompletable(..)
    , Candidates
    , NatSortChunk(..)
    , NatSortKey(..)
    , ConsoleCommand(..)
    , Priority
    , Cache(..)
    , Capacity
    , CommandLiteral(..)
    , Resources(..)
    , ImageSupport(..)
    , ImagePreview(..)
    , Viewports(..)
    , Extents(..)
    , mkCapacity
    , LogChan(LogChan)
    , ProcHandle(..)
    , Opts(..)
    , ImagePreviewer(..)
    , CommandMacro(..)
    , SoftDeps
    , Urgency(..)
    , Echoed(..)
    , Loading(..)
    ) where

import           Brick                   ( AttrMap, Extent )
import           Brick.BChan             ( BChan )
import           Brick.Focus             ( FocusRing )
import           Brick.Types             ( Viewport, Widget )
import           Brick.Widgets.Edit      ( Editor )
import           Brick.Widgets.List      ( List )

import           Control.Concurrent      ( MVar, ThreadId )
import           Control.Concurrent.STM  ( TBQueue )
import           Control.Exception       ( Exception )
import           Control.Monad           ( MonadPlus(mzero) )
import           Control.Monad.Logger    ( LogLevel(..), LogLine )
import           Control.Monad.Reader    ( ReaderT )

import           Data.Aeson
                 ( (.!=)
                 , (.:?)
                 , FromJSON(parseJSON)
                 , Options(constructorTagModifier)
                 , ToJSON
                 , defaultOptions
                 , genericParseJSON
                 , withObject
                 )
import           Data.Aeson.Casing       ( trainCase )
import           Data.Char               ( toLower )
import           Data.Function           ( on )
import           Data.Generics.Labels    ()
import           Data.HashMap.Strict     ( HashMap )
import           Data.HashPSQ            ( HashPSQ )
import           Data.Hashable           ( Hashable )
import           Data.Int                ( Int64 )
import           Data.List.NonEmpty      ( NonEmpty )
import           Data.Map.Strict         ( Map )
import           Data.Sequence           ( Seq )
import           Data.String             ( IsString )
import qualified Data.Text               as T
import           Data.Text               ( Text )
import           Data.Text.Zipper        ( TextZipper )
import           Data.Time               ( TimeZone )
import           Data.Vector             ( Vector )

import           GHC.Generics            ( Generic )

import qualified Graphics.Vty            as Vty
import           Graphics.Vty            ( Key(KChar), Modifier )

import           Lens.Micro.Platform

import           Skylighting             ( Syntax, SyntaxMap )

import qualified System.FilePath         as F
import           System.FilePath         ( (</>) )
import           System.IO               ( Handle )
import           System.Posix
                 ( COff
                 , EpochTime
                 , FileMode
                 , FileOffset
                 , GroupID
                 )
import           System.Posix.ByteString ( UserID )
import           System.Process          ( ProcessHandle )

data AppEnv = AppEnv
    { config               :: MVar Config
    , resources            :: MVar Resources
    , cwd                  :: MVar AbsPath
    , history              :: MVar History
    , fm                   :: MVar FileManager
    , bindingMode          :: MVar BindingMode
    , modeHints            :: MVar (Maybe ModeHints)
    , console              :: MVar Console
    , consoleEditor        :: MVar (Editor Text Name)
    , searchResults        :: MVar (Maybe SearchResults)
    , yanked               :: MVar [Yanked]
    , viewports            :: Maybe Viewports
    , extents              :: Maybe Extents
    , loading              :: Loading
    , popup                :: MVar (Maybe (Widget Name))
    , currentBindingPrefix :: MVar Text
    , echoed               :: MVar (Maybe Echoed)
    }
    deriving ( Generic )

data AppState = AppState
    { env                  :: AppEnv
    , config               :: Config
    , resources            :: Resources
    , cwd                  :: AbsPath
    , fm                   :: FileManager
    , modeHints            :: Maybe ModeHints
    , console              :: Console
    , consoleEditor        :: Editor Text Name
    , searchResults        :: Maybe SearchResults
    , yanked               :: [Yanked]
    , popup                :: Maybe (Widget Name)
    , currentBindingPrefix :: Text
    , bindingMode          :: BindingMode
    , viewports            :: Maybe Viewports
    , extents              :: Maybe Extents
    , echoed               :: Maybe Echoed
    , loader               :: Maybe Char
    }
    deriving ( Generic )

data Name = BrowserList | ParentList | PreviewArea | ConsoleEditor
    deriving ( Show, Eq, Ord )

data Event
    = QuitNorris
    | WatchFs
    | FsChange AbsPath
    | SuspendAndResume (ReaderT AppEnv IO ())
    | Load Char
    | Loaded

data Viewports = Viewports { browser :: Viewport, preview :: Viewport }
    deriving ( Generic )

data Extents = Extents { preview :: Extent Name }
    deriving ( Generic )

data Loading = Loading { browser :: MVar ThreadId, preview :: MVar ThreadId }
    deriving ( Generic )

newtype AbsPath = AbsPath { unAbsPath :: FilePath }
    deriving ( Generic, Show, Eq, Ord, IsString, Hashable, ToJSON, Semigroup
             , Monoid )

class Eq a => IsFilePath a where
    toFilePath :: a -> FilePath
    fromFilePath :: FilePath -> a
    isEmpty :: a -> Bool

    (<//>) :: a -> a -> a
    infixr 5 <//>

    (<//>) x y = fromFilePath $ ((</>) `on` toFilePath) x y

    pathName :: a -> Text
    pathName (isEmpty -> True) = mempty
    pathName fp = getLast fp
      where
        getLast = T.pack . last . F.splitPath . toFilePath

instance IsFilePath FilePath where
    toFilePath = id

    fromFilePath = id

    isEmpty = null

instance IsFilePath AbsPath where
    toFilePath (AbsPath fp) = fp

    fromFilePath = AbsPath

    isEmpty (AbsPath fp) = isEmpty fp

pattern EvChar :: Char -> [Modifier] -> KeyEvent
pattern EvChar k m = (Vty.EvKey (KChar k) m)

type KeyEvent = Vty.Event

data BindingMode = NormalMode | VisualMode | PendingMode Pending
    deriving ( Eq, Ord )

data Pending
    = GlobalPrefix
    | LineNumPrefix (NonEmpty Int)
    | SettingsPrefix
    | YankPrefix
    | PutPrefix
    | DeletePrefix
    | BookmarkPrefix
    | GotoBookmarkPrefix
    | UndoPrefix (Maybe Undoable)
    | TagRegisterPrefix
    deriving ( Eq, Ord )

data Undoable = UndoBookmark
    deriving ( Eq, Ord )

type ModeHints = Map Text Text

type BindingHints = Map BindingMode ModeHints

data Echoed = Echoed { message :: Text, urgency :: Urgency }
    deriving Generic

data Urgency = Message | Error

-- File manager
data Browser = Browser
    { total :: Int, size :: COff, entries :: List Name Entry, dirHash :: Int }
    deriving ( Generic )

data Entry = File { item :: Item } | Dir { item :: Item }
    deriving ( Generic, Eq )

type Entries = Vector Entry

data Item = Item
    { path     :: AbsPath
    , name     :: Text
    , metadata :: MetaData
    , marked   :: Bool
    , tag      :: Maybe Tag
    }
    deriving ( Generic, Eq )

data MetaData = MetaData
    { mode     :: FileMode
    , size     :: FileOffset
    , accessed :: EpochTime
    , modified :: EpochTime
    , owner    :: Username
    , group    :: Groupname
    , isHidden :: Bool
    , fileType :: FileType
    , count    :: Maybe Int
    }
    deriving ( Generic, Eq )

data History = History
    { recent         :: Recent
    , usernameCache  :: NameCache Username
    , groupnameCache :: NameCache Groupname
    , browserCache   :: BrowserCache
    , savedPositions :: SavedPositions
    , asciiImgCache  :: AsciiImgCache
    }
    deriving ( Generic )

type NameCache a = Map Int a

type Recent = Seq AbsPath

type BrowserCache = Cache AbsPath Browser

type SavedPositions = HashMap AbsPath AbsPath

type AsciiImgCache = Map AbsPath Text

data Owner = User UserID | Group GroupID

type Username = Text

type Groupname = Text

data FileType
    = RegularFile
    | Directory
    | SymLink
    | NamedPipe
    | BlockDevice
    | CharacterDevice
    | Socket
    deriving ( Eq )

newtype MimeType = MimeType Text
    deriving ( Generic, Eq, IsString )

data FileManager = FileManager
    { browser :: Maybe Browser
    , parent  :: Maybe Browser
    , preview :: Maybe Preview
    }
    deriving ( Generic )

newtype Extension = Extension Text
    deriving ( Generic, Eq, IsString, Hashable )

type MimeMap = HashMap Extension MimeType

data Executable = Executable
    { name :: Text, exePath :: AbsPath, defaultFlags :: [ProcessFlag] }
    deriving ( Generic, Eq )

instance IsFilePath Executable where
    toFilePath = (^. #exePath . to toFilePath)

    fromFilePath fp = Executable { .. }
      where
        name         = pathName fp

        exePath      = AbsPath fp

        defaultFlags = mempty

    isEmpty = (^. #exePath . to isEmpty)

newtype DirTree = DirTree [Child]
    deriving ( Show, Generic, Eq, Hashable, Semigroup, Monoid )

data Child = Child AbsPath | ChildTree DirTree
    deriving ( Show, Generic, Eq )

instance Hashable Child

data Preview
    = DirContents (Maybe Browser)
    | FileContentsHL Syntax Text
    | FileContentsPlain Text
    | FileInfo FInfo
    | Unavailable
    | Img ImagePreview
    | ImgAscii Text

newtype FInfo = FInfo { unFInfo :: Text }
    deriving ( Generic )

-- Config
data Config = Config
    { attrMap          :: AttrMap
    , userInfo         :: UserInfo
    , settings         :: Settings
    , syntaxMap        :: SyntaxMap
    , bindingHints     :: BindingHints
    , clipboardSupport :: Maybe ClipboardSupport
    , imageSupport     :: Maybe ImageSupport
    , mimeMap          :: MimeMap
    , bookmarks        :: Bookmarks
    , xdgDirs          :: XdgDirs
    , tags             :: Tags
    , hardDeps         :: HardDeps
    , softDeps         :: SoftDeps
    , cacheCap         :: Capacity Int
    , logFile          :: AbsPath
    , clean            :: Bool
    }
    deriving ( Generic )

data Resources = Resources
    { focusRing        :: FocusRing Name
    , eventChan        :: BChan Event
    , logChan          :: LogChan LogLine
    , logWorkerRunning :: MVar ()
    }
    deriving ( Generic )

data Opts = Opts
    { configPath :: Maybe FilePath
    , cacheDir   :: Maybe FilePath
    , wd         :: Maybe FilePath
    , clean      :: Bool
    }
    deriving ( Generic )

newtype LogChan a = LogChan (TBQueue a)
    deriving ( Generic )

data UserInfo = UserInfo
    { username :: Text
    , host     :: Text
    , timeZone :: TimeZone
    , homeDir  :: AbsPath
    , editor   :: Executable
    , pager    :: Executable
    , shell    :: Text
    }
    deriving ( Generic )

data Settings = Settings
    { showHidden           :: Bool
    , sortDirFirst         :: Bool
    , sortOrder            :: SortOrder
    , iCaseSearch          :: Bool
    , maxPreviewLines      :: Int
    , maxPreviewSize       :: COff
    , highlightStyle       :: HighlightStyle
    , fallbackApplications :: FallbackApplications
    , shouldWriteLog       :: Bool
    , logLevel             :: LogLevel
    , imagePreview         :: Maybe ImagePreviewer
    }
    deriving ( Generic )

instance FromJSON Settings where
    parseJSON = withObject "Settings" $ \s -> do
        showHidden <- s .:? "show-hidden" .!= (defs ^. #showHidden)
        sortDirFirst <- s .:? "sort-dir-first" .!= (defs ^. #sortDirFirst)
        sortOrder <- s .:? "sort-order" .!= (defs ^. #sortOrder)
        iCaseSearch <- s .:? "icase-search" .!= (defs ^. #iCaseSearch)
        maxPreviewLines <- s .:? "max-preview-lines"
            .!= (defs ^. #maxPreviewLines)
        maxPreviewSize <- s .:? "max-preview-size" >>= \case
            Just mps -> return $ fromIntegral @Int mps
            Nothing  -> return $ defs ^. #maxPreviewSize
        highlightStyle <- s .:? "hl-style" .!= (defs ^. #highlightStyle)
        shouldWriteLog <- s .:? "write-log" .!= (defs ^. #shouldWriteLog)
        imagePreview <- s .:? "image-preview" .!= (defs ^. #imagePreview)
        logLevel <- s .:? "log-level" >>= \case
            Just (lvl :: Text) -> case lvl of
                "debug" -> return LevelDebug
                "info"  -> return LevelInfo
                "warn"  -> return LevelWarn
                "error" -> return LevelError
                _       -> mzero
            Nothing            -> return $ defs ^. #logLevel
        return $ Settings { .. }
      where
        defs                 = defaultSettings

        fallbackApplications = defs ^. #fallbackApplications

defaultSettings :: Settings
defaultSettings = Settings
    { showHidden           = False
    , sortDirFirst         = True
    , sortOrder            = AlphAsc
    , iCaseSearch          = True
    , maxPreviewLines      = 50
    , maxPreviewSize       = 1024 ^ (2 :: Int)
    , highlightStyle       = Monochrome
    , shouldWriteLog       = False
    , logLevel             = LevelWarn
    , imagePreview         = Nothing
    , fallbackApplications = FallbackApplications
          { pager  = "less"
          , editor = "vim" -- blasphemy
          }
    }

data Bookmark = Bookmark { char :: Char, path :: AbsPath, persistent :: Bool }
    deriving ( Generic )

type Bookmarks = Map Char Bookmark

newtype Tag = Tag { unTag :: Char }
    deriving ( Generic, Eq )

type Tags = Map AbsPath Tag

data XdgDirs =
    XdgDirs { dataDir :: AbsPath, confDir :: AbsPath, cacheDir :: AbsPath }
    deriving ( Generic )

type HardDeps = Map Text Executable

type SoftDeps = Map Text Executable

data ClipboardSupport = Xclip Provider | Xsel Provider
    deriving ( Generic )

data ImagePreviewer = W3m | Ueberzug
    deriving ( Generic )

instance FromJSON ImagePreviewer where
    parseJSON = genericParseJSON
        $ defaultOptions { constructorTagModifier = fmap toLower }

data ImageSupport =
    ImageSupport { previewer :: ImagePreviewer, provider :: Provider }
    deriving ( Generic )

data ImagePreview = ImagePreview { cleanup :: [Char], handle :: ProcHandle }
    deriving ( Generic )

data ProcHandle = ProcHandle
    { stdin   :: Handle
    , stdout  :: Maybe Handle
    , stderr  :: Maybe Handle
    , process :: ProcessHandle
    }
    deriving ( Generic )

data Provider = Provider { exePath :: Executable, defaultArgs :: [[Char]] }
    deriving ( Generic )

data NatSortChunk = Numeric Integer Int | Textual [(Char, Char)]
    deriving ( Eq, Ord )

newtype NatSortKey = NatSortKey [NatSortChunk]
    deriving ( Eq, Ord )

data FallbackApplications =
    FallbackApplications { pager :: Text, editor :: Text }
    deriving ( Generic )

data SortOrder
    = AlphAsc
    | AlphDesc
    | SizeAsc
    | SizeDesc
    | ModifiedAsc
    | ModifiedDesc
    deriving ( Generic )

instance FromJSON SortOrder where
    parseJSON = genericParseJSON
        $ defaultOptions { constructorTagModifier = trainCase }

data HighlightStyle
    = Kate
    | BreezeDark
    | Pygments
    | Espresso
    | Tango
    | Haddock
    | Monochrome
    | Zenburn
    deriving ( Generic, Read )

instance FromJSON HighlightStyle where
    parseJSON = genericParseJSON
        $ defaultOptions { constructorTagModifier = trainCase }

-- UI
data Console = Console
    { inputMode :: Maybe InputMode
    , search    :: Input SearchText
    , command   :: Input CommandText
    }
    deriving ( Generic )

data InputMode = SearchPrompt | CommandPrompt | InputPrompt UserPrompt
    deriving ( Generic )

data Input a = Input
    { history       :: Seq a
    , scrollback    :: Maybe (Scrollback a)
    , tabCompletion :: Maybe (TabCompletion a)
    }
    deriving ( Generic )

newtype SearchText = SearchText { unSearchText :: Text }
    deriving ( Generic )

newtype CommandText = CommandText { unCommandText :: Text }
    deriving ( Generic )

data UserPrompt = UserPrompt
    { prompt    :: Text
    , predicate :: Text -> Bool
    , mkCommand :: Text -> Command
    }
    deriving ( Generic )

data Scrollable a where
    SearchScroll :: SearchText -> Scrollable SearchText
    CommandScroll :: CommandText -> Scrollable CommandText

data Scrollback a = Scrollback { position :: Int, items :: Seq Text }
    deriving ( Generic )

data TabCompletion a =
    TabCompletion { position :: Int, candidates :: [Text], prefix :: Text }
    deriving ( Generic )

type family Candidates a = b | b -> a where
    Candidates SearchText = [Entry]
    Candidates CommandText = [CommandLiteral]

class TabCompletable a where
    candidatesToText :: Candidates a -> [Text]
    mkPrefix :: a -> Text
    newTabCompletion :: Candidates a -> a -> TabCompletion a
    newTabCompletion cs x = TabCompletion { .. }
      where
        prefix     = mkPrefix x

        candidates = candidatesToText cs

        position   = 0

instance TabCompletable SearchText where
    candidatesToText es = es ^.. each . #item . #name

    mkPrefix = (^. #unSearchText)

instance TabCompletable CommandText where
    candidatesToText = fmap unLit
      where
        unLit (CmdChar c) = T.singleton c
        unLit (CmdText t) = t

    mkPrefix = (^. #unCommandText)

data SearchResults = SearchResults
    { searchText :: SearchText, results :: Seq Entry, position :: Int }
    deriving ( Generic )

data Direction = Up | Down

data ShellCmd = ShellCmd { flags :: [ProcessFlag], cmd :: Text }
    deriving ( Generic )

data ProcessFlag = Fork | NeedsTerm
    deriving ( Eq, Ord )

data Command
    = Control ControlCommand
    | Navigation NavCommand
    | FileOp FileOpCommand
    | Toggle SettingsCommand
    | Visual VisualCommand

data ControlCommand
    = Quit
    | Reload
    | Shell ShellCmd
    | Echo Text
    | ChangeBindingMode BindingMode
    | ChangeFocus Name
    | MakeBookmark Char
    | UnBookmark Char
    | ToggleMark
    | UnmarkAll
    | ToggleTag (Maybe Char)
    | Untag
    | ScrollSearchRes Direction
    | ConsoleCmd ConsoleCommand

data ConsoleCommand
    = EnterConsole InputMode
    | LeaveConsole
    | SubmitInput InputMode
    | ScrollConsoleHist Direction InputMode
    | TabComplete InputMode
    | ApplyEdit InputMode (TextZipper Text -> TextZipper Text)

data NavCommand = Cd Destination | Goto Position

data FileOpCommand
    = Yank YankOptions
    | Delete DeleteOptions
    | Put PutOptions
    | Open
    | OpenWith (Maybe Opener)
    | Rename (Maybe Renamed)
    | BulkRename
    | Touch FilePath
    | MkDir FilePath

data VisualCommand = VDown | VUp | VTop | VBottom

data YankOptions = YankPathToClip | YankEntry

data DeleteOptions = ConsoleDelete (Maybe [Entry]) | Cut

data PutOptions = Overwrite | Prepend

data SettingsCommand
    = ShowHidden
    | SortBy SortOrder
    | SortDirFirst
    | UseHLStyle HighlightStyle

data Renamed = Renamed { entry :: Entry, new :: Text }
    deriving ( Generic )

data Destination
    = Home
    | Root
    | Parent
    | MostRecent
    | PrevChild
    | NextChild
    | Fp FilePath
    | Bookmarked Char

data Position
    = Top
    | Middle
    | Bottom
    | Page Direction
    | HalfPage Direction
    | Line Direction
    | LineNumber Int
    | LineOffset (Browser -> Maybe Int)

data Yanked = Yanked { move :: Bool, entry :: Entry }
    deriving ( Generic )

data Opener = Opener { exe :: Text, entry :: Entry }
    deriving ( Generic )

data Popup a = Popup { item :: a, draw :: a -> Widget Name, title :: Text }
    deriving ( Generic )

data CommandLiteral = CmdChar Char | CmdText Text
    deriving ( Eq )

data CommandMacro
    = PathAtPoint
    | CwdPath
    | SelectedNames
    | SelectedPaths
    | CwdTags

-- Cache
data Cache k v = Cache
    { cap   :: Capacity Int
    , size  :: Int
    , tick  :: Priority
    , queue :: HashPSQ k Priority v
    }
    deriving ( Eq, Generic )

newtype Capacity a = Capacity a
    deriving ( Eq, Ord, Generic, Num, Real, Enum, Integral )

mkCapacity :: Integral a => a -> Maybe (Capacity a)
mkCapacity x
    | x >= 1 = Just $ Capacity x
    | otherwise = Nothing

type Priority = Int64

-- Error
data Error
    = FsError IOError
    | MiscError Text
    | CommandError Text
    | ProcessError IOError
    deriving ( Show )

instance Exception Error
