-- |
module Norris.Effects
    ( module U
    , modify_
    , swap_
    , reactFSChange
    , leaveCwd
    , runControlCommand
    , runSettingsComand
    , runFileOpCommand
    , runNavCommand
    , xdgFile
    , runVisualCommand
    , loadXdgData
    , tabCompleteSearch
    , lookupOwners
    , mkScroll
    , fmEvent
    , cmpCached
    , consoleEvent
    , clearTabCompletion
    , clearScrollback
    , tabCompleteCommand
    , clearConsole
    , leaveConsole
    , initSession
    , showLoader
    , setBrowser
    , setParent
    ) where

import qualified Brick                            as B
import           Brick                            ( (<+>), (<=>) )
import qualified Brick.Widgets.List               as B
import qualified Brick.Widgets.Skylighting        as B

import           Control.Exception                ( Exception(displayException)
                                                  )
import           Control.Monad
                 ( filterM
                 , foldM
                 , forever
                 , unless
                 , void
                 , when
                 )
import           Control.Monad.Extra              ( whenJust )
import           Control.Monad.Logger
                 ( MonadLogger
                 , logDebugN
                 , logErrorN
                 )

import           Data.Bool                        ( bool )
import           Data.Foldable                    ( for_, traverse_ )
import           Data.Hashable                    ( Hashable(hash) )
import qualified Data.Map.Strict                  as M
import           Data.Maybe                       ( fromMaybe, maybeToList )
import           Data.Sequence                    ( (<|) )
import qualified Data.Text                        as T
import           Data.Text                        ( Text, isPrefixOf )
import qualified Data.Text.Zipper                 as Z
import qualified Data.Vector                      as V

import           Lens.Micro.Platform

import           Norris.Config                    ( toSlStyle )
import           Norris.Effects.Common            as U
                 ( catchSh_
                 , currentBrowser
                 , currentParent
                 , errMessage
                 , getHardDep
                 , handleMaybe
                 , lookupDep
                 , lookupHardDep
                 , lookupSoftDep
                 , markedOrCurrentEntry
                 , modifyEntries
                 , newBrowser
                 , retag
                 )
import           Norris.Effects.Common
                 ( getCurrentEntry
                 , getCurrentIdx
                 , reloadAll
                 , reloadCwd
                 , runIfCurrentEntry
                 , runMarksOrCurrent
                 )
import           Norris.Effects.Config.Class      ( MonadConfig(..) )
import           Norris.Effects.Etc.Class
                 ( MonadErr(displayError)
                 , MonadPreview(loadNewPreview)
                 , MonadProcess(..)
                 , MonadResource(..)
                 )
import           Norris.Effects.FileManager       as U
                 ( copyDir
                 , copyFile
                 , deleteDir
                 , deleteFile
                 , getExecutableForMimeType
                 , getMimeType
                 , mkEntry
                 , newPreview
                 )
import           Norris.Effects.FileManager       ( bulkRename, getChild )
import           Norris.Effects.FileManager.Class
                 ( MonadBrowser(..)
                 , MonadFile(..)
                 , MonadHistory(..)
                 , MonadNav(..)
                 , yankPathToClip
                 )
import           Norris.Effects.UI.Class
                 ( MonadConsole(..)
                 , MonadUI(..)
                 )
import           Norris.FileManager
                 ( abbrevHomeDir
                 , currentEntry
                 , findParentPath
                 , isParentOf
                 , markedEntries
                 , recPositions
                 , selectChild
                 )
import qualified Norris.Types
import           Norris.Types
                 ( AbsPath(AbsPath)
                 , BindingMode(..)
                 , Browser
                 , Command(..)
                 , CommandLiteral(CmdChar, CmdText)
                 , CommandMacro(..)
                 , CommandText(CommandText)
                 , Console
                 , ConsoleCommand(..)
                 , ControlCommand(..)
                 , DeleteOptions(..)
                 , Destination(..)
                 , Direction(..)
                 , Entries
                 , Error(FsError)
                 , Event(..)
                 , FileOpCommand(..)
                 , History
                 , Input
                 , InputMode(..)
                 , IsFilePath((<//>), pathName, toFilePath)
                 , KeyEvent
                 , Name(BrowserList)
                 , NameCache
                 , NavCommand(..)
                 , Opener(Opener)
                 , Pending(..)
                 , Popup(Popup)
                 , Position(..)
                 , Renamed(Renamed)
                 , Scrollable(CommandScroll, SearchScroll)
                 , SettingsCommand(..)
                 , TabCompletable(newTabCompletion)
                 , TabCompletion
                 , Tag(Tag)
                 , Undoable(UndoBookmark)
                 , Urgency(Message)
                 , UserPrompt(UserPrompt)
                 , VisualCommand(..)
                 , XdgDirs
                 , YankOptions(..)
                 , Yanked(Yanked)
                 )
import           Norris.UI.Command
                 ( defaultTag
                 , descMode
                 , mkBindingCommand
                 , mkConsoleCommand
                 , specialBookmarkChar
                 )
import           Norris.UI.Command.Parser         ( validCommands )
import           Norris.UI.Console
                 ( mkCommandText
                 , mkScrollback
                 , mkSearchText
                 )
import           Norris.Utils
                 ( cacheToList
                 , exeToCmd
                 , quote
                 , scrollWithBounds
                 )

import           Prelude                          hiding ( readFile )

import           UnliftIO
                 ( MVar
                 , MonadIO
                 , MonadUnliftIO
                 , catch
                 , modifyMVar_
                 , swapMVar
                 , timeout
                 )
import           UnliftIO.Concurrent              ( threadDelay )
import qualified UnliftIO.Directory               as D

modify_ :: MonadUnliftIO m => (a -> m a) -> MVar a -> m ()
modify_ = flip modifyMVar_

swap_ :: MonadUnliftIO m => a -> MVar a -> m ()
swap_ x m = void $ swapMVar m x

xdgFile :: MonadConfig m => Lens' XdgDirs AbsPath -> AbsPath -> m FilePath
xdgFile l path = toFilePath
    <$> ((<//>) <$> focusConfig (#xdgDirs . l) <*> pure path)

showLoader :: (MonadIO m, MonadResource m) => m ()
showLoader = void . forever . for_ [ '/', '-', '\\', '|' ] $ \c -> do
    threadDelay 40000
    writeEvChan $ Load c

loadXdgData :: (MonadConfig m, MonadConsole m, MonadNav m) => m ()
loadXdgData = tryLoad "tagged" verifyTags
    >> tryLoad "bookmarks" verifyBookmarks
    >> tryLoad "searched" (modifyConsole #search . const)
    >> tryLoad "commands" (modifyConsole #command . const)
  where
    tryLoad path g = readXdgData path >>= traverse_ g

    verifyTags ts = do
        existing <- filterM pathExists (M.toList ts)
        modifyConfig #tags (const $ M.fromList existing)
      where
        pathExists (path, _) = doesPathExist $ toFilePath path

    verifyBookmarks bs = do
        existing <- filterM pathExists (M.toList bs)
        modifyConfig #bookmarks (const $ M.fromList existing)
      where
        pathExists (_, bm) = bm ^. #path & toFilePath & doesPathExist

dumpXdgData :: (MonadConfig m, MonadConsole m) => m ()
dumpXdgData = (bookmarks >>= writeXdgData "bookmarks")
    >> (tags >>= writeXdgData "tagged")
    >> (currentConsole <&> (^. #search) >>= writeXdgData "searched")
    >> (currentConsole <&> (^. #command) >>= writeXdgData "commands")

runControlCommand
    :: ( MonadUI m
       , MonadNav m
       , MonadConsole m
       , MonadConfig m
       , MonadHistory m
       , MonadBrowser m
       , MonadProcess m
       , MonadResource m
       )
    => ControlCommand
    -> m ()
runControlCommand = \case
    Quit -> dumpXdgData >> writeEvChan QuitNorris
    Reload -> reloadCwd
    Echo txt -> echo Message txt
    Shell shCmd -> lookupDep exeName >>= \case
        Just exe -> runSh_ $ exeToCmd exe args
        Nothing  -> runSh_ shCmd
      where
        (exeName, args) = shCmd ^. #cmd & T.breakOn " "

    ChangeBindingMode bm -> setBindingMode bm >> case bm of
        VisualMode -> toggleMark
        PendingMode BookmarkPrefix -> do
            item <- mkBookmarks
            showPopup $ Popup { .. }
        PendingMode GotoBookmarkPrefix -> do
            item <- mkBookmarks
            showPopup $ Popup { .. }
        PendingMode (UndoPrefix (Just UndoBookmark)) -> do
            item <- mkBookmarks
            showPopup $ Popup { .. }
        mode -> do
            bindingHints <- focusConfig #bindingHints
            for_ (bindingHints ^. at mode) $ \item -> showPopup $ Popup { .. }
      where
        mkBookmarks  = do
            home <- focusConfig $ #userInfo . #homeDir
            M.map (^. #path . to (abbrevHomeDir home) . #unAbsPath . packed)
                . M.mapKeys T.singleton
                <$> bookmarks

        title        = descMode bm

        draw mode = M.foldrWithKey' foldMs (B.txt mempty) mode

        foldMs k v a = ((B.hLimitPercent 10 $ B.txtWrap k) <+> B.txt v) <=> a

    ChangeFocus n -> changeFocus n
    MakeBookmark k -> bookmark k
    UnBookmark k -> unBookmark k
    ToggleMark -> toggleMark >> modifyEntries B.listMoveDown
    UnmarkAll -> do
        browserM <- currentBrowser
        for_ browserM $ \browser -> do
            let marked = markedEntries browser
            idx <- getCurrentIdx
            for_ marked $ \en -> modifyEntries (B.listMoveToElement en)
                >> toggleMark
            whenJust idx (modifyEntries . B.listMoveTo)
    Untag -> runIfCurrentEntry $ toggleTag Nothing
    ToggleTag Nothing ->
        runIfCurrentEntry $ \en -> toggle en >> modifyEntries B.listMoveDown
      where
        toggle entry = case entry ^. #item . #tag of
            Just _  -> toggleTag Nothing entry
            Nothing -> toggleTag (Just defaultTag) entry
    ToggleTag (Just k) -> toggle >> modifyEntries B.listMoveDown
      where
        toggle = runIfCurrentEntry . toggleTag . Just $ Tag k
    ScrollSearchRes d -> gotoResult d

    ConsoleCmd c -> case c of
        LeaveConsole -> leaveConsole
        SubmitInput SearchPrompt -> doSearch =<< getConsoleText mkSearchText
        SubmitInput CommandPrompt ->
            doCommand =<< expandCommandMacros =<< getConsoleText mkCommandText
        SubmitInput (InputPrompt input) ->
            parsePromptInput input >> leaveConsole
        EnterConsole im -> enterConsole im
        ScrollConsoleHist direction im -> scroll direction im
        ApplyEdit im tz -> clearTabCompletion im >> setConsoleText tz
        TabComplete im -> tabComplete im

runNavCommand
    :: ( MonadPreview m
       , MonadUI m
       , MonadBrowser m
       , MonadHistory m
       , MonadUnliftIO m
       , MonadErr m
       , MonadConfig m
       , MonadNav m
       )
    => NavCommand
    -> m ()
runNavCommand = \case
    Goto loc -> navigate >> loadNewPreview
      where
        navigate = case loc of
            Top                  -> moveTo 0
            Middle               ->
                traverse_ (moveTo . (\b -> b ^. #total & (`div` 2)))
                =<< currentBrowser
            Bottom               -> traverse_ (moveTo . (^. #total))
                =<< currentBrowser
            Line Up              -> modifyEntries B.listMoveUp
            Line Down            -> modifyEntries B.listMoveDown
            Page Up              -> moveByPage (-1)
            Page Down            -> moveByPage 1
            HalfPage Up          -> moveByPage (-0.5)
            HalfPage Down        -> moveByPage 0.5
            LineNumber n         -> moveTo n
            LineOffset getOffset -> traverse_ (traverse_ moveTo . getOffset)
                =<< currentBrowser
          where
            moveTo n = modifyEntries (B.listMoveTo n)

    Cd d -> navigate >> loadNewPreview
      where
        gotoChild n = do
            child <- getChild n
            for_ child $ \c -> cd $ c ^. #item . #path

        navigate    = case d of
            Home         -> cd =<< (focusConfig $ #userInfo . #homeDir)
            Root         -> cd "/"
            MostRecent   -> popRecent
            Parent       -> do
                cwd <- viewCwd
                traverse_ cd (findParentPath cwd)
            PrevChild    -> gotoChild (-1)
            NextChild    -> gotoChild 1
            Fp fp        -> do
                path <- D.canonicalizePath fp
                doesPathExist fp >>= \case
                    True  -> cd $ AbsPath path
                    False -> errMessage
                        $ fp ^. packed . to quote
                        <> ": "
                        <> "path does not exist"
            Bookmarked k -> do
                ms <- bookmarks
                for_ (ms ^? at k . _Just . #path)
                    $ \m -> doesPathExist (toFilePath m) >>= \case
                        True  -> bookmark specialBookmarkChar >> cd m
                        False -> unBookmark k

runFileOpCommand
    :: ( MonadUnliftIO m
       , MonadBrowser m
       , MonadFile m
       , MonadConsole m
       , MonadErr m
       , MonadHistory m
       , MonadNav m
       , MonadProcess m
       , MonadConfig m
       , MonadLogger m
       )
    => FileOpCommand
    -> m ()
runFileOpCommand f = runOp `catch` \e ->
    (logErrorN . T.pack $ displayException e) >> (displayError $ FsError e)
  where
    runOp = case f of
        Open -> runIfCurrentEntry open
        OpenWith Nothing -> runIfCurrentEntry $ \entry ->
            let prompt        = "Open with: "
                mkCommand exe = FileOp . OpenWith . Just $ Opener { .. }
                predicate     = const True
                userPrompt    = UserPrompt { .. }
            in
                enterConsole $ InputPrompt userPrompt
        OpenWith (Just o) -> openWith o
        Yank y -> case y of
            YankPathToClip -> runIfCurrentEntry $ \en ->
                yankPathToClip (en ^. #item . #path)
            YankEntry      -> clearYanked
                >> runMarksOrCurrent (yankPath . Yanked False)
        Put p -> yanked >>= \case
            [] -> errMessage "Nothing to paste"
            ys -> do
                for_ ys $ \y -> let en = (y ^. #entry)
                                in
                                    bool (copy p en) (move p en) (y ^. #move)
                reloadCwd
                    >> clearYanked
                    >> traverse_ yankPath
                                 (ys ^.. each . filtered (^. #move . to not))
        Delete d -> case d of
            Cut -> clearYanked >> runMarksOrCurrent (yankPath . Yanked True)
            ConsoleDelete (Just es) -> traverse_ delete es >> reloadCwd
            ConsoleDelete Nothing -> marks >>= \case
                [] -> runIfCurrentEntry $ \entry -> do
                    let prompt    = mkPrompt $ entry ^. #item . #name
                        mkCommand = cmd $ Just [ entry ]
                        confirm   = UserPrompt { .. }
                    enterConsole $ InputPrompt confirm
                es -> do
                    let prompt    = mkPrompt
                            $ T.intercalate ", " (es ^.. each . #item . #name)
                        mkCommand = cmd $ Just es
                        confirm   = UserPrompt { .. }
                    enterConsole $ InputPrompt confirm
          where
            predicate inp = inp `elem` [ "y", "yes" ]

            cmd           = const . FileOp . Delete . ConsoleDelete

            mkPrompt x = mconcat [ "Really delete ", x, "? (y/N) " ]
        Rename Nothing -> runIfCurrentEntry $ \entry ->
            let prompt        = mconcat [ "Enter New name for "
                                        , entry ^. #item . #name
                                        , ": "
                                        ]
                mkCommand new = FileOp . Rename . Just $ Renamed { .. }
                predicate     = const True
                userPrompt    = UserPrompt { .. }
            in
                enterConsole $ InputPrompt userPrompt
        Rename (Just r) -> do
            idx <- getCurrentIdx
            rename r
                >> reloadCwd
                >> traverse_ (modifyEntries . B.listMoveTo) idx
        BulkRename -> bulkRename =<< entries
          where
            entries = marks >>= \case
                [] -> maybeToList <$> getCurrentEntry
                xs -> return xs
        Touch fp -> touch fp >> reloadCwd
        MkDir fp -> mkdir fp >> reloadCwd

runSettingsComand
    :: (MonadConfig m, MonadHistory m, MonadBrowser m, MonadNav m)
    => SettingsCommand
    -> m ()
runSettingsComand = \case
    ShowHidden    -> toggleSettings #showHidden not >> reloadAll
    SortBy so     -> toggleSettings #sortOrder (const so) >> reloadAll
    SortDirFirst  -> toggleSettings #sortDirFirst not >> reloadAll
    UseHLStyle hl -> do
        toggleSettings #highlightStyle (const hl)
        modifyConfig #attrMap $ \am ->
            B.applyAttrMappings (B.attrMappingsForStyle $ toSlStyle hl) am

runVisualCommand :: MonadBrowser m => VisualCommand -> m ()
runVisualCommand = \case
    VDown   -> modifyEntries B.listMoveDown >> toggleMark
    VUp     -> modifyEntries B.listMoveUp >> toggleMark
    VTop    -> visualRegion $ \idx es ->
        for_ ((es & V.splitAt (idx + 1)) ^. _1)
        $ \_ -> modifyEntries B.listMoveUp >> toggleMark
    VBottom -> visualRegion $ \idx es ->
        for_ ((es & V.splitAt (idx + 1)) ^. _2)
        $ \_ -> modifyEntries B.listMoveDown >> toggleMark

visualRegion :: MonadBrowser m => (Int -> Entries -> m ()) -> m ()
visualRegion g = do
    browserM <- currentBrowser
    for_ browserM $ \browser ->
        whenJust (browser ^. #entries . B.listSelectedL) $ \idx ->
        g idx (browser ^. #entries . B.listElementsL)

lookupOwners :: (Integral a, MonadHistory m)
             => (a -> m b)
             -> (b -> [Char])
             -> Lens' History (NameCache Text)
             -> a
             -> m Text
lookupOwners g h l x = do
    names <- focusHistory l
    maybe lookupName return (names ^. at asInt)
  where
    lookupName = do
        name <- T.pack . h <$> g x
        saveOwner l asInt name >> return name

    asInt      = fromIntegral x

saveOwner
    :: MonadHistory m => Lens' History (NameCache Text) -> Int -> Text -> m ()
saveOwner l n t = modifyHistory l (at n ?~ t)

mkScroll :: MonadConsole m
         => Direction
         -> ([Text] -> a)
         -> (a -> Scrollable a)
         -> Lens' Console (Input a)
         -> m ()
mkScroll direction inputType toScrollable l = do
    txt <- getConsoleText inputType
    scrollText <- scrollConsoleHistory direction (toScrollable txt) l
    for_ scrollText $ \t -> setConsoleText (Z.insertMany t . Z.clearZipper)

scrollConsoleHistory
    :: MonadConsole m
    => Direction
    -> Scrollable a
    -> Lens' Console (Input a)
    -> m (Maybe Text)
scrollConsoleHistory direction inp l = do
    console <- currentConsole
    let scrollback =
            fromMaybe (mkScrollback inp console) (console ^. l . #scrollback)
        history    = scrollback ^. #items
        pos        =
            scrollback ^. #position & scrollWithBounds direction history
        saved      = history ^? ix pos

    modifyConsole (l . #scrollback)
                  (const . Just $ scrollback & #position .~ pos)
        >> return saved

parsePromptInput :: (MonadConsole m, MonadUI m) => UserPrompt -> m ()
parsePromptInput up = do
    input <- getConsoleText (T.toLower . T.strip . T.concat)
    when (predicate input) (run input)
  where
    predicate = up ^. #predicate

    run inp = runCommand $ (up ^. #mkCommand) inp

reactFSChange
    :: (MonadBrowser m, MonadNav m, MonadHistory m) => AbsPath -> m ()
reactFSChange path = do
    cwd <- viewCwd
    uncacheBrowser path
        >> traverse_ uncacheBrowser (findParentPath path)
        >> when (or [ path == cwd
                    , Just path == findParentPath cwd
                    , cwd `isParentOf` path
                    ])
                reloadCwd

moveByPage :: (MonadUI m, MonadBrowser m) => Double -> m ()
moveByPage pages = do
    browserM <- currentBrowser
    for_ browserM $ \browser -> do
        vp <- viewports <&> (^? _Just . #browser)
        for_ vp $ \v -> do
            let size = v ^. B.vpSize . _2
                nes  = round
                    $ pages * fromIntegral size
                    / fromIntegral (browser ^. #entries . B.listItemHeightL)
            modifyEntries (B.listMoveBy nes)

cmpCached
    :: (MonadUnliftIO m, MonadHistory m, MonadResource m, MonadBrowser m)
    => m ()
cmpCached = do
    paths <- focusHistory #browserCache <&> cacheToList
    for_ paths $ \(path, _, br) ->
        timeout 100000 $ handleMaybe mkDirTreeImmediate path >>= \case
            Nothing -> writeEvChan $ FsChange path
            Just tr -> unless (br ^. #dirHash == hash tr)
                              (writeEvChan (FsChange path))

fmEvent :: MonadUI m => KeyEvent -> m ()
fmEvent evc = getBindingMode >>= \case
    n@NormalMode      -> case mkBindingCommand n evc of
        Just cmd@(Control (ChangeBindingMode VisualMode)) -> runCommand cmd
        Just cmd@(Control (ChangeBindingMode _)) ->
            showBindingPrefix evc >> runCommand cmd
        cmd -> traverse_ runCommand cmd
    p@(PendingMode _) -> case mkBindingCommand p evc of
        Just cmd@(Control (ChangeBindingMode _)) ->
            showBindingPrefix evc >> runCommand cmd
        cmd -> traverse_ runCommand cmd >> resetBindingMode
    v@VisualMode      ->
        maybe resetBindingMode runCommand (mkBindingCommand v evc)

consoleEvent :: (MonadNav m, MonadConsole m, MonadUI m) => KeyEvent -> m ()
consoleEvent evc = currentConsole <&> (^. #inputMode)
    >>= traverse_ (runCommand . (`mkConsoleCommand` evc))

leaveCwd :: (MonadHistory m, MonadBrowser m, MonadNav m) => m ()
leaveCwd = do
    cwd <- viewCwd
    parent <- currentParent
    for_ ((,) <$> findParentPath cwd <*> parent) $ \(path, p) ->
        cacheBrowser path p
        >> traverse_ (savePosition path)
                     (p ^? #entries . to currentEntry . _Just . #item . #path)
    runIfCurrentEntry $ \en -> savePosition cwd (en ^. #item . #path)
    traverse_ (cacheBrowser cwd) =<< currentBrowser
    modifyHistory #recent (cwd <|)

resetBindingMode :: MonadUI m => m ()
resetBindingMode = setBindingMode NormalMode >> hidePopup >> hideBindingPrefix

tabCompleteSearch :: (MonadConsole m, MonadBrowser m) => m ()
tabCompleteSearch = currentConsole <&> (^. #search . #tabCompletion) >>= \case
    Nothing -> do
        browserM <- currentBrowser
        for_ browserM $ \browser -> do
            txt <- getConsoleText mkSearchText
            let entries       = browser ^. #entries . B.listElementsL
                candidates    = case txt ^. #unSearchText of
                    t
                        | T.null t -> entries ^.. each
                        | otherwise -> entries
                            ^.. each . filtered (matchName txt)
                tabCompletion = newTabCompletion candidates txt
            setNewTabCompletion #search tabCompletion
    Just tc -> runTabComplete #search tc
  where
    matchName stxt en = stxt ^. #unSearchText
        & (`isPrefixOf` (en ^. #item . #name))

tabCompleteCommand :: MonadConsole m => m ()
tabCompleteCommand =
    currentConsole <&> (^. #command . #tabCompletion) >>= \case
        Nothing -> do
            txt <- getConsoleText mkCommandText
            let candidates    = case txt ^. #unCommandText of
                    t
                        | T.null t -> validCommands ^.. each
                        | otherwise -> validCommands
                            ^.. each . filtered (matchName txt)
                tabCompletion = newTabCompletion candidates txt
            setNewTabCompletion #command tabCompletion
        Just tc -> runTabComplete #command tc
  where
    matchName ctxt cmd = ctxt ^. #unCommandText & (`isPrefixOf` unCmd cmd)

    unCmd (CmdText t) = t
    unCmd (CmdChar c) = T.singleton c

runTabComplete
    :: MonadConsole m => Lens' Console (Input a) -> TabCompletion a -> m ()
runTabComplete l tc = case tc ^? #candidates . ix (tc ^. #position) of
    Nothing -> case tc ^? #candidates . _head of
        Just t  -> insertTabCompletion t
            >> incrementTabCompletionPos l (const 1)
        Nothing -> return ()
    Just t  -> insertTabCompletion t >> incrementTabCompletionPos l succ

setNewTabCompletion
    :: MonadConsole m => Lens' Console (Input a) -> TabCompletion a -> m ()
setNewTabCompletion l tc = case tc ^? #candidates . _head of
    Just t  -> modifyConsole (l . #tabCompletion) (const $ Just tc)
        >> insertTabCompletion t
        >> incrementTabCompletionPos l succ
    Nothing -> return ()

insertTabCompletion :: MonadConsole m => Text -> m ()
insertTabCompletion t = setConsoleText (Z.insertMany t . Z.clearZipper)

incrementTabCompletionPos
    :: MonadConsole m => Lens' Console (Input a) -> (Int -> Int) -> m ()
incrementTabCompletionPos l g =
    modifyConsole (l . #tabCompletion) (_Just . #position %~ g)

clearTabCompletion :: MonadConsole m => InputMode -> m ()
clearTabCompletion = \case
    SearchPrompt  -> modifyConsole (#search . #tabCompletion) (const Nothing)
    CommandPrompt -> modifyConsole (#command . #tabCompletion) (const Nothing)
    _             -> return ()

clearScrollback :: MonadConsole m => InputMode -> m ()
clearScrollback = \case
    SearchPrompt  -> modifyConsole (#search . #scrollback) (const Nothing)
    CommandPrompt -> modifyConsole (#command . #scrollback) (const Nothing)
    _             -> return ()

clearConsole :: MonadConsole m => m ()
clearConsole = do
    currentConsole <&> (^. #inputMode)
        >>= (`for_` (\im -> clearScrollback im >> clearTabCompletion im))
    modifyConsole #inputMode (const Nothing) >> setConsoleText Z.clearZipper

leaveConsole :: (MonadConsole m, MonadResource m) => m ()
leaveConsole = clearConsole >> changeFocus BrowserList

scroll :: MonadConsole m => Direction -> InputMode -> m ()
scroll direction = \case
    SearchPrompt  -> mkScroll direction mkSearchText SearchScroll #search
    CommandPrompt -> mkScroll direction mkCommandText CommandScroll #command
    _             -> return ()

tabComplete :: (MonadConsole m, MonadBrowser m) => InputMode -> m ()
tabComplete = \case
    SearchPrompt  -> tabCompleteSearch
    CommandPrompt -> tabCompleteCommand
    _             -> return ()

expandCommandMacros :: (MonadBrowser m, MonadNav m, MonadConfig m)
                    => CommandText
                    -> m CommandText
expandCommandMacros (CommandText ctxt) =
    CommandText <$> foldM expand ctxt macros
  where
    macros = [ PathAtPoint, CwdPath, SelectedNames, SelectedPaths, CwdTags ]

    expand !txt macro = do
        ex <- expandMacro macro
        return $! T.replace (macroText macro) ex txt

    expandMacro PathAtPoint = getCurrentEntry >>= \case
        Just entry -> return $ entry ^. #item . #name
        Nothing    -> return mempty
    expandMacro CwdPath = T.pack . toFilePath <$> viewCwd
    expandMacro SelectedNames =
        markedOrCurrentEntry <&> (^.. each . #item . #name) <&> T.unwords
    expandMacro SelectedPaths = markedOrCurrentEntry
        <&> (^.. each . #item . #path . to toFilePath . to T.pack)
        <&> T.unwords
    expandMacro CwdTags = do
        cwd <- viewCwd
        tags
            <&> M.filterWithKey (\path _ -> cwd `isParentOf` path)
            <&> M.foldrWithKey' pathNames mempty
      where
        pathNames path _ ns = ns <> " " <> pathName path

    macroText = \case
        PathAtPoint   -> "%f"
        CwdPath       -> "%d"
        SelectedNames -> "%s"
        SelectedPaths -> "%p"
        CwdTags       -> "%t"

initSession :: ( MonadBrowser m
               , MonadNav m
               , MonadConfig m
               , MonadConsole m
               , MonadPreview m
               , MonadHistory m
               , MonadResource m
               , MonadLogger m
               )
            => m ()
initSession = do
    cwd <- viewCwd
    logChan <- focusResource #logChan
    browser <- newBrowser BrowserList cwd
    parent <- findParent cwd
    loadXdgData
        >> runLogWorker logChan
        >> setBrowser browser
        >> setParent (selectChild cwd <$> parent)
        >> loadNewPreview
        >> traverse_ (uncurry savePosition) (recPositions cwd)
        >> logDebugN "norris-jr started"

setBrowser :: MonadBrowser m => Browser -> m ()
setBrowser browser = modifyFm #browser (const $ Just browser)

setParent :: MonadBrowser m => Maybe Browser -> m ()
setParent parent = modifyFm #parent (const parent)

toggleMark :: MonadBrowser m => m ()
toggleMark = modifyEntries (B.listModify (#item . #marked %~ not))
