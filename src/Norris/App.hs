{-# LANGUAGE RecordWildCards #-}

-- |
module Norris.App
    ( AppState(..)
    , AppEnv(AppEnv)
    , Config(Config)
    , UserInfo(..)
    , updateAppState
    , newAppEnv
    , applicationName
    ) where

import           Brick.BChan               ( BChan )
import qualified Brick.Focus               as B
import qualified Brick.Themes              as B
import qualified Brick.Widgets.Edit        as B

import           Control.Applicative       ( Alternative((<|>)) )
import           Control.Concurrent        ( newEmptyMVar, newMVar, readMVar )
import           Control.Concurrent.STM    ( atomically, newTBQueue )
import           Control.Exception         ( throwIO )
import           Control.Monad.IO.Class    ( MonadIO(liftIO) )
import           Control.Monad.Trans.Maybe ( MaybeT(MaybeT, runMaybeT) )

import qualified Data.Map.Strict           as M
import           Data.Maybe                ( catMaybes, fromMaybe )
import qualified Data.Text                 as T
import qualified Data.Time                 as TM
import           Data.Traversable          ( for )
import qualified Data.Yaml                 as Y

import           Lens.Micro.Platform       hiding ( preview )

import           Norris.Config             ( toSlStyle )
import           Norris.FileManager        ( defaultMimeMap, emptyBrowser )
import qualified Norris.Types
import           Norris.Types
                 ( AppEnv(..)
                 , AppState(..)
                 , BindingMode(NormalMode)
                 , Capacity
                 , ClipboardSupport(..)
                 , Config(..)
                 , Event
                 , Executable(Executable)
                 , FallbackApplications
                 , FileManager(FileManager)
                 , FileManager(FileManager)
                 , HardDeps
                 , ImagePreviewer(Ueberzug, W3m)
                 , ImageSupport(..)
                 , IsFilePath(toFilePath, (<//>), fromFilePath)
                 , Loading(Loading)
                 , LogChan(LogChan)
                 , Name(ConsoleEditor, BrowserList, ParentList)
                 , Opts
                 , ProcessFlag(..)
                 , Provider(Provider)
                 , Resources(Resources)
                 , SoftDeps
                 , UserInfo(..)
                 , XdgDirs(XdgDirs)
                 , defaultSettings
                 , mkCapacity
                 )
import           Norris.UI.Command         ( defaultBindingHints )
import           Norris.UI.Console         ( newConsole )
import           Norris.Utils
                 ( defaultTheme
                 , emptyHistory
                 , quote
                 , updateSyntaxAttrs
                 )

import qualified Skylighting.Syntax        as SL

import qualified System.Directory          as D
import           System.Directory          ( XdgDirectory(..) )
import           System.Environment        ( getEnv, lookupEnv )
import qualified System.Posix              as PX

newAppEnv :: BChan Event -> Opts -> Int -> IO AppEnv
newAppEnv eventChan opts cap = do
    cacheCap <- maybe (throwIO
                       $ userError "Norris.Types.Cache: capacity must be > 0")
                      return
                      (mkCapacity cap)

    config <- newConfig opts cacheCap >>= newMVar
    resources <- newResources eventChan >>= newMVar

    cwd <- newMVar . fromFilePath
        =<< maybe D.getCurrentDirectory D.canonicalizePath (opts ^. #wd)

    fm <- newMVar newFm
    history <- newMVar $ emptyHistory cacheCap
    bindingMode <- newMVar NormalMode
    console <- newMVar newConsole
    consoleEditor <- newMVar $ B.editorText ConsoleEditor (Just 1) mempty
    modeHints <- newMVar Nothing
    searchResults <- newMVar Nothing
    yanked <- newMVar mempty
    loading <- newLoading
    popup <- newMVar Nothing
    currentBindingPrefix <- newMVar mempty
    echoed <- newMVar Nothing
    return AppEnv { viewports = Nothing, extents = Nothing, .. }
  where
    newLoading = do
        browser <- newEmptyMVar
        preview <- newEmptyMVar
        return Loading { .. }

updateAppState :: MonadIO m => AppEnv -> m AppState
updateAppState env = do
    config <- liftIO . readMVar $ env ^. #config
    resources <- liftIO . readMVar $ env ^. #resources
    cwd <- liftIO . readMVar $ env ^. #cwd
    fm <- liftIO . readMVar $ env ^. #fm
    modeHints <- liftIO . readMVar $ env ^. #modeHints
    console <- liftIO . readMVar $ env ^. #console
    consoleEditor <- liftIO . readMVar $ env ^. #consoleEditor
    searchResults <- liftIO . readMVar $ env ^. #searchResults
    yanked <- liftIO . readMVar $ env ^. #yanked
    popup <- liftIO . readMVar $ env ^. #popup
    !_ <- liftIO . readMVar $ env ^. #history
    currentBindingPrefix <- liftIO . readMVar $ env ^. #currentBindingPrefix
    bindingMode <- liftIO . readMVar $ env ^. #bindingMode
    echoed <- liftIO . readMVar $ env ^. #echoed
    return AppState { .. }
  where
    viewports = env ^. #viewports

    extents   = env ^. #extents

    loader    = Nothing

newConfig :: Opts -> Capacity Int -> IO Config
newConfig opts cacheCap = do
    clipboardSupport <- getClipboardSupport
    xdgDirs <- getXdgDirs opts
    userInfo <- getUserInfo $ defaultSettings ^. #fallbackApplications
    hardDeps <- getHardDeps userInfo
    softDeps <- getSoftDeps
    settings <- do
        location <- case opts ^. #configPath of
            Just loc -> D.canonicalizePath loc
            Nothing  ->
                return . toFilePath $ xdgDirs ^. #confDir <//> "norris.conf"
        D.doesFileExist location >>= \case
            False -> return defaultSettings
            True  -> Y.decodeFileThrow location

    imageSupport <- case settings ^. #imagePreview of
        Nothing -> return Nothing
        Just ip -> getImageSupport ip

    let syntaxMap    = SL.defaultSyntaxMap
        bindingHints = defaultBindingHints
        logFile      = xdgDirs ^. #cacheDir <//> "norris.log"
        attrMap      =
            updateSyntaxAttrs (settings ^. #highlightStyle & toSlStyle)
                              (B.themeToAttrMap defaultTheme)
        mimeMap      = defaultMimeMap
        bookmarks    = mempty
        tags         = mempty
        clean        = opts ^. #clean
    return Config { .. }

newResources :: BChan Event -> IO Resources
newResources eventChan = do
    logChan <- atomically $ LogChan <$> newTBQueue 1000
    logWorkerRunning <- newMVar ()
    return Resources { .. }
  where
    focusRing = B.focusRing [ BrowserList, ParentList, ConsoleEditor ]

newFm :: FileManager
newFm = FileManager (Just emptyBrowser) Nothing Nothing

getClipboardSupport :: IO (Maybe ClipboardSupport)
getClipboardSupport = D.findExecutable "xclip" >>= \case
    Just path -> return . Just . Xclip
        $ Provider (fromFilePath path) [ "-selection", "c" ]
    Nothing   -> do
        xsel <- D.findExecutable "xsel"
        return $ xsel >>= \path -> Just . Xsel
            $ Provider (fromFilePath path) [ "-i", "-b" ]

getImageSupport :: ImagePreviewer -> IO (Maybe ImageSupport)
getImageSupport previewer = traverse mkImgSupport
    =<< D.findExecutable (exeName previewer)
  where
    mkImgSupport fp =
        return ImageSupport
               { previewer, provider = Provider (fromFilePath fp) mempty }

    exeName W3m      = "w3mimgdisplay"
    exeName Ueberzug = "ueberzug"

getXdgDirs :: Opts -> IO XdgDirs
getXdgDirs opts =
    XdgDirs <$> getXdg XdgData <*> getXdg XdgConfig <*> handleCache
  where
    getXdg xdgD = do
        fp <- D.getXdgDirectory xdgD applicationName
        D.createDirectoryIfMissing False fp
            >> PX.setFileMode fp perms700
            >> (return $ fromFilePath fp)

    perms700    = PX.nullFileMode `PX.unionFileModes` PX.ownerModes

    handleCache = case opts ^. #cacheDir of
        Nothing -> getXdg XdgCache
        Just fp -> do
            cacheD <- D.canonicalizePath fp
            D.doesDirectoryExist cacheD >>= \case
                True  -> return $ fromFilePath cacheD
                False -> throwIO . userError
                    $ "The provided cache directory "
                    <> cacheD
                    <> " does not exist"

getUserInfo :: FallbackApplications -> IO UserInfo
getUserInfo fs = do
    timeZone <- TM.getCurrentTimeZone
    editor <- getEditor fs
    pager <- getPager fs
    host <- T.pack <$> getEnv "HOST" <|> return "host"

    ue <- PX.getRealUserID >>= PX.getUserEntryForID

    let username = ue ^. to PX.userName . packed
        homeDir  = ue ^. to PX.homeDirectory & fromFilePath
        shell    = ue ^. to PX.userShell . packed

    return UserInfo { .. }

getEditor :: FallbackApplications -> IO Executable
getEditor fs = do
    edPath <- runMaybeT (MaybeT . D.findExecutable =<< MaybeT lookupEd)
        <&> fromMaybe (fs ^. #editor . unpacked)
    return $ fromFilePath edPath & #defaultFlags .~ [ NeedsTerm ]
  where
    lookupEd = (<|>) <$> lookupEnv "VISUAL" <*> lookupEnv "EDITOR"

getPager :: FallbackApplications -> IO Executable
getPager fs = do
    pagerPath
        <- runMaybeT (MaybeT . D.findExecutable =<< MaybeT (lookupEnv "PAGER"))
        <&> fromMaybe (fs ^. #pager . unpacked)
    return $ fromFilePath pagerPath & #defaultFlags .~ [ NeedsTerm ]

getHardDeps :: UserInfo -> IO HardDeps
getHardDeps uinfo = do
    deps <- for [ "xdg-mime", "file" ] $ \dep -> D.findExecutable dep >>= \case
        Just exePath ->
            return ( name
                   , Executable
                     { exePath = fromFilePath exePath, name, defaultFlags }
                   )
          where
            name         = T.pack dep

            defaultFlags = mempty
        Nothing -> throwIO . userError $ notFoundMsg dep
    return . M.fromList $ deps <> [ editorEntry ] <> [ pagerEntry ]
  where
    notFoundMsg name =
        mconcat [ "The dependency "
                , quote name
                , " was not found in your $PATH. "
                , "Please install it to continue using "
                , applicationName
                ]

    editorEntry      = (uinfo ^. #editor . #name, uinfo ^. #editor)

    pagerEntry       = (uinfo ^. #pager . #name, uinfo ^. #pager)

getSoftDeps :: IO SoftDeps
getSoftDeps = do
    deps <- for [ "pdftotext", "convert", "jp2a" ]
        $ \dep -> D.findExecutable dep >>= \case
            Just exePath -> return
                $ Just ( name
                       , Executable
                         { exePath      = fromFilePath exePath
                         , name
                         , defaultFlags
                         }
                       )
              where
                name         = T.pack dep

                defaultFlags = mempty
            Nothing -> return Nothing
    return . M.fromList $ catMaybes deps

applicationName :: [Char]
applicationName = "norris-jr"
