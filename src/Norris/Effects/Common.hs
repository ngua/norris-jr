-- |
module Norris.Effects.Common
    ( newBrowser
    , getCurrentEntry
    , getCurrentIdx
    , runMarksOrCurrent
    , runIfCurrentEntry
    , handleMaybe
    , catchSh_
    , reloadCwd
    , retag
    , reloadAll
    , currentParent
    , currentBrowser
    , markedOrCurrentEntry
    , getHardDep
    , lookupDep
    , lookupSoftDep
    , lookupHardDep
    , errMessage
    , modifyEntries
    ) where

import qualified Brick.Widgets.List               as B
import           Brick.Widgets.List               ( List )

import           Data.Foldable                    ( for_, traverse_ )
import           Data.Hashable                    ( Hashable(hash) )
import qualified Data.Map.Strict                  as M
import           Data.Map.Strict                  ( (!) )
import           Data.Maybe                       ( maybeToList )
import           Data.Text                        ( Text )

import           Lens.Micro.Platform

import           Norris.Config                    ( sortEntries )
import           Norris.Effects.Config.Class
                 ( MonadConfig(focusConfig, tags, modifyConfig)
                 )
import           Norris.Effects.Etc.Class
                 ( MonadErr(displayError, catchIOErr_)
                 )
import           Norris.Effects.FileManager.Class
import           Norris.FileManager
                 ( currentEntry
                 , filterHiddenEntries
                 , findParentPath
                 , mkEntryList
                 , selectChild
                 , wdSize
                 )
import qualified Norris.Types
import           Norris.Types
                 ( AbsPath
                 , Browser(Browser)
                 , Entry(File, Dir)
                 , Error(MiscError, FsError)
                 , Executable
                 , Name
                 , ShellCmd
                 )
import           Norris.Utils                     ( cacheLookup, emptyCache )

import           System.Exit                      ( ExitCode(ExitFailure) )

import           UnliftIO
                 ( MonadUnliftIO
                 , handleIO
                 , throwIO
                 )

handleMaybe :: MonadUnliftIO m => (a -> m b) -> a -> m (Maybe b)
handleMaybe g x = handleIO (\_ -> return Nothing) (Just <$> g x)

errMessage :: MonadErr m => Text -> m ()
errMessage = displayError . MiscError

currentBrowser :: MonadBrowser m => m (Maybe Browser)
currentBrowser = currentFm <&> (^. #browser)

currentParent :: MonadBrowser m => m (Maybe Browser)
currentParent = currentFm <&> (^. #parent)

getHardDep :: MonadConfig m => Text -> m Executable
getHardDep name = focusConfig #hardDeps <&> (! name)

lookupHardDep :: MonadConfig m => Text -> m (Maybe Executable)
lookupHardDep name = focusConfig #hardDeps <&> (^. at name)

lookupSoftDep :: MonadConfig m => Text -> m (Maybe Executable)
lookupSoftDep name = focusConfig #softDeps <&> (^. at name)

lookupDep :: MonadConfig m => Text -> m (Maybe Executable)
lookupDep name = allDeps <&> (^. at name)
  where
    allDeps = M.union <$> focusConfig #hardDeps <*> focusConfig #softDeps

getCurrentEntry :: MonadBrowser m => m (Maybe Entry)
getCurrentEntry = do
    browserM <- currentBrowser
    return $ (^. #entries . to currentEntry) =<< browserM

getCurrentIdx :: MonadBrowser m => m (Maybe Int)
getCurrentIdx = do
    browserM <- currentBrowser
    return $ (^. #entries . to B.listSelected) =<< browserM

markedOrCurrentEntry :: MonadBrowser m => m [Entry]
markedOrCurrentEntry = marks >>= \case
    [] -> maybeToList <$> getCurrentEntry
    xs -> return xs

runMarksOrCurrent :: MonadBrowser m => (Entry -> m ()) -> m ()
runMarksOrCurrent g = traverse_ g =<< markedOrCurrentEntry

runIfCurrentEntry :: MonadBrowser m => (Entry -> m ()) -> m ()
runIfCurrentEntry g = traverse_ g =<< getCurrentEntry

catchSh_ :: (MonadUnliftIO m, MonadErr m)
         => (ShellCmd -> m ExitCode)
         -> ShellCmd
         -> m ()
catchSh_ g cmd = run `catchIOErr_` FsError
  where
    run = g cmd >>= \case
        ExitFailure _ -> throwIO . userError
            $ "Failed to launch " <> cmd ^. #cmd . unpacked
        _             -> return ()

reloadCwd :: (MonadBrowser m, MonadHistory m, MonadNav m) => m ()
reloadCwd = do
    cwd <- viewCwd
    runIfCurrentEntry $ \en -> savePosition cwd (en ^. #item . #path)
        >> reloadPreview en
    uncacheBrowser cwd
        >> traverse_ uncacheBrowser (findParentPath cwd)
        >> loadNewBrowser cwd
  where
    reloadPreview (Dir d)  = uncacheBrowser (d ^. #path)
    reloadPreview (File _) = return ()

reloadAll
    :: (MonadHistory m, MonadBrowser m, MonadNav m, MonadConfig m) => m ()
reloadAll = do
    cacheCap <- focusConfig #cacheCap
    modifyHistory #browserCache (const $ emptyCache cacheCap) >> reloadCwd

retag :: (MonadNav m, MonadConfig m) => AbsPath -> AbsPath -> m ()
retag oldPath newPath = do
    oldTag <- tags <&> (^. at oldPath)
    for_ oldTag $ \t -> modifyConfig #tags (at oldPath .~ Nothing)
        >> modifyConfig #tags (at newPath ?~ t)

newBrowser :: (MonadBrowser m, MonadHistory m, MonadConfig m)
           => Name
           -> AbsPath
           -> m Browser
newBrowser name path = do
    cached <- focusHistory #browserCache
    browser <- do
        case cacheLookup path cached of
            Nothing           -> mkNew
            Just (br, cache') -> modifyHistory #browserCache (const cache')
                >> gotoLastLoc br
    cacheBrowser path browser >> return browser
  where
    gotoLastLoc br = do
        let entries = br ^. #entries . B.listElementsL
            browser = br & #entries .~ mkEntryList name entries
        savedPosition <- lookupPosition path
        return $ maybe browser (`selectChild` browser) savedPosition

    mkNew          = do
        allEntries <- getEntries path
        settings <- focusConfig #settings
        dirHash <- hash <$> mkDirTreeImmediate path
        let total      = length rawEntries
            size       = wdSize allEntries
            rawEntries = filterHiddenEntries settings . sortEntries settings
                $ allEntries
            entries    = mkEntryList name rawEntries
            browser    = Browser { .. }
        return browser

modifyEntries
    :: MonadBrowser m => (List Name Entry -> List Name Entry) -> m ()
modifyEntries g = modifyFm #browser (fmap (#entries %~ g))
