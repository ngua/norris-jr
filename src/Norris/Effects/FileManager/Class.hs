-- |
module Norris.Effects.FileManager.Class
    ( MonadBrowser(..)
    , MonadHistory(..)
    , MonadNav(..)
    , MonadFile(..)
    ) where

import           Data.Text           ( Text )

import           Lens.Micro.Platform ( Lens' )

import           Norris.Types
                 ( AbsPath
                 , Browser
                 , DirTree
                 , Entries
                 , Entry(..)
                 , FileManager
                 , History
                 , Opener
                 , Owner
                 , PutOptions
                 , Renamed
                 , Yanked
                 )

class Monad m => MonadNav m where
    viewCwd :: m AbsPath
    setCwd :: AbsPath -> m ()
    cd :: AbsPath -> m ()
    verifyAccess :: AbsPath -> m Bool
    doesPathExist :: FilePath -> m Bool
    isDir :: FilePath -> m Bool
    mkAbsPath :: FilePath -> FilePath -> m AbsPath

class Monad m => MonadBrowser m where
    type E m

    modifyFm :: Lens' FileManager a -> (a -> a) -> m ()
    listDir :: AbsPath -> m (Either (E m) [AbsPath])
    getEntries :: AbsPath -> m Entries
    findParent :: AbsPath -> m (Maybe Browser)
    loadNewBrowser :: AbsPath -> m ()
    currentFm :: m FileManager
    yanked :: m [Yanked]
    marks :: m [Entry]
    mkDirTreeImmediate :: AbsPath -> m DirTree

class Monad m => MonadHistory m where
    readHistory :: m History
    modifyHistory :: Lens' History a -> (a -> a) -> m ()
    focusHistory :: Lens' History a -> m a
    cacheBrowser :: AbsPath -> Browser -> m ()
    uncacheBrowser :: AbsPath -> m ()
    popRecent :: m ()
    savePosition :: AbsPath -> AbsPath -> m ()
    lookupPosition :: AbsPath -> m (Maybe AbsPath)
    getOwners :: Owner -> m Text

class Monad m => MonadFile m where
    copy :: PutOptions -> Entry -> m ()
    move :: PutOptions -> Entry -> m ()
    delete :: Entry -> m ()
    touch :: FilePath -> m ()
    mkdir :: FilePath -> m ()
    yankPath :: Yanked -> m ()
    yankPathToClip :: AbsPath -> m ()
    clearYanked :: m ()
    open :: Entry -> m ()
    openWith :: Opener -> m ()
    rename :: Renamed -> m ()
