-- |
module Norris.Effects.Config.Class ( MonadConfig(..) ) where

import           Lens.Micro    ( (<&>), (^.), Lens' )

import           Norris.Config ( XdgData )
import           Norris.Types
                 ( AbsPath
                 , Bookmarks
                 , Config
                 , Entry
                 , Settings
                 , Tag
                 , Tags
                 )

class Monad m => MonadConfig m where
    readConfig :: m Config
    focusConfig :: Lens' Config a -> m a
    modifyConfig :: Lens' Config a -> (a -> a) -> m ()
    bookmark :: Char -> m ()
    unBookmark :: Char -> m ()
    bookmarks :: m Bookmarks
    writeXdgData :: XdgData a => AbsPath -> a -> m ()
    readXdgData :: XdgData a => AbsPath -> m (Maybe a)
    tags :: m Tags
    toggleTag :: Maybe Tag -> Entry -> m ()
    toggleSettings :: Lens' Settings a -> (a -> a) -> m ()

    focusSettings :: Lens' Settings a -> m a
    focusSettings l = focusConfig #settings <&> (^. l)
