-- |
module Norris.Effects.FileManager
    ( dirCount
    , mkEntry
    , getChild
    , newPreview
    , prependToPath
    , bulkRename
    , copyFile
    , copyDir
    , deleteDir
    , deleteFile
    , getMimeType
    , getExecutableForMimeType
    ) where

import qualified Brick                            as B
import qualified Brick.Widgets.List               as B

import           Control.Monad                    ( when )
import           Control.Monad.Extra              ( maybeM )
import           Control.Monad.IO.Class           ( MonadIO(liftIO) )
import           Control.Monad.Trans.Maybe

import           Data.Aeson
                 ( Value(String, Number, Object)
                 , encode
                 )
import           Data.Bool                        ( bool )
import           Data.Foldable                    ( for_ )
import           Data.Function                    ( on )
import           Data.Generics.Product            ( HasPosition(position) )
import           Data.List                        ( intercalate )
import qualified Data.Text                        as T
import           Data.Text                        ( Text )
import qualified Data.Text.Encoding               as T
import qualified Data.Text.IO                     as T
import qualified Data.Text.Lazy                   as LT
import qualified Data.Text.Lazy.Encoding          as LT

import           GHC.Exts                         ( IsList(fromList) )

import           Graphics.Vty                     ( DisplayRegion )

import           Lens.Micro.Platform              hiding ( preview )

import           Norris.Effects.Common
                 ( catchSh_
                 , currentParent
                 , errMessage
                 , getHardDep
                 , handleMaybe
                 , lookupSoftDep
                 , modifyEntries
                 , newBrowser
                 , reloadCwd
                 , retag
                 )
import           Norris.Effects.Config.Class
                 ( MonadConfig(focusConfig, tags, focusSettings)
                 )
import           Norris.Effects.Etc.Class
                 ( MonadErr(catchIOErr_)
                 , MonadPreview(readFile, getFileInfo)
                 , MonadProcess(..)
                 )
import           Norris.Effects.FileManager.Class
                 ( MonadBrowser(currentFm)
                 , MonadHistory(..)
                 , MonadNav(mkAbsPath, verifyAccess, doesPathExist, isDir, viewCwd)
                 )
import           Norris.Effects.UI.Class          ( MonadUI(extents) )
import           Norris.FileManager
                 ( entryNames
                 , fromDesktopEntry
                 , getFileType
                 , isHidden
                 , mkExtension
                 , mkMimeType
                 , selectChild
                 )
import qualified Norris.Types
import           Norris.Types
                 ( AbsPath(AbsPath)
                 , Entry(Dir, File)
                 , Error(FsError)
                 , Executable
                 , FileType(RegularFile)
                 , ImagePreview
                 , ImagePreview(ImagePreview)
                 , ImagePreviewer(W3m, Ueberzug)
                 , ImageSupport(..)
                 , IsFilePath(pathName, toFilePath)
                 , Item(Item)
                 , MetaData(MetaData)
                 , MimeType
                 , Name(PreviewArea)
                 , Owner(Group, User)
                 , Preview(..)
                 , PutOptions(Overwrite, Prepend)
                 , ShellCmd
                 )
import           Norris.Utils                     ( defaultShCmd, expandTabs )

import           Prelude                          hiding ( readFile )

import qualified Skylighting                      as SL

import qualified Streaming.ByteString.Char8       as Q

import           System.Exit
                 ( ExitCode(ExitSuccess, ExitFailure)
                 )
import           System.FilePath                  ( (</>) )
import           System.IO                        ( hFlush, hPutStr )
import qualified System.Posix                     as PX
import           System.Posix                     ( FileStatus )
import           System.Process                   ( StdStream(CreatePipe) )

import           Text.Read                        ( readMaybe )

import           UnliftIO
                 ( MonadUnliftIO
                 , hClose
                 , throwIO
                 , withSystemTempFile
                 )
import qualified UnliftIO.Directory               as D
import qualified UnliftIO.Process                 as P
import           UnliftIO.Process                 ( terminateProcess )
import           UnliftIO.Resource                ( runResourceT )

mkEntry :: ( MonadUnliftIO m
           , MonadHistory m
           , MonadBrowser m
           , MonadNav m
           , MonadConfig m
           )
        => AbsPath
        -> m (Maybe Entry)
mkEntry = handleMaybe mkEn
  where
    mkEn path@(AbsPath fp) = do
        fs <- liftIO $ PX.getFileStatus fp
        isD <- isDir fp
        metadata <- getMetadata fs path isD
        tag <- tags <&> (^. at path)
        let ty     = bool File Dir isD
            name   = pathName path
            marked = False
            item   = Item { .. }
        return $ ty item

getMetadata :: (MonadBrowser m, MonadHistory m, MonadUnliftIO m)
            => FileStatus
            -> AbsPath
            -> Bool
            -> m MetaData
getMetadata fs path isD = do
    count <- bool (return Nothing) (dirCount path) isD
    owner <- getOwners (User $ PX.fileOwner fs)
    group <- getOwners (Group $ PX.fileGroup fs)
    return MetaData
           { size     = PX.fileSize fs
           , mode     = PX.fileMode fs
           , accessed = PX.accessTime fs
           , modified = PX.modificationTime fs
           , isHidden = isHidden path
           , fileType = getFileType fs
           , count
           , owner
           , group
           }

getChild :: MonadBrowser m => Int -> m (Maybe Entry)
getChild n = runMaybeT $ do
    parent <- MaybeT currentParent
    idx <- MaybeT . return $ parent ^. #entries . B.listSelectedL
    MaybeT . return
        $ parent ^? #entries . B.listElementsL . ix (idx + n) >>= \case
            File _       -> Nothing
            next@(Dir _) -> parent
                ^? #entries . traversed . filtered (matchPath next)
  where
    matchPath = (==) `on` (^. #item . #path)

newPreview :: ( MonadConfig m
              , MonadPreview m
              , MonadBrowser m
              , MonadHistory m
              , MonadNav m
              , MonadProcess m
              , MonadUI m
              , MonadUnliftIO m
              )
           => Entry
           -> m Preview
newPreview en = currentFm <&> (^. #preview) >>= \case
    Just (Img im) -> do
        liftIO $ hPutStr (im ^. #handle . #stdin) (im ^. #cleanup)
        terminateProcess (im ^. #handle . #process)
        mkPr en
    _             -> mkPr en

mkPr :: ( MonadConfig m
        , MonadPreview m
        , MonadBrowser m
        , MonadHistory m
        , MonadNav m
        , MonadProcess m
        , MonadUI m
        , MonadUnliftIO m
        )
     => Entry
     -> m Preview
mkPr (Dir d) = verifyAccess path >>= \case
    True  -> do
        browser <- newBrowser PreviewArea path
        saved <- lookupPosition path
        return . DirContents . Just
            $ maybe browser (`selectChild` browser) saved
    False -> return $ DirContents Nothing
  where
    path = d ^. #path
mkPr (File f) = do
    access <- verifyAccess path
    if
        | (access && ftype == RegularFile) -> getMimeType path >>= \case
            Just mt
                | mt == "application/pdf" -> previewPdf
                | mt `elem` [ "image/jpeg", "image/png" ] -> previewImg mt
                | otherwise -> defaultRead
            _       -> defaultRead
        | otherwise -> fileDefault
  where
    ftype = f ^. #metadata . #fileType

    fsize = f ^. #metadata . #size

    path = f ^. #path

    readIt = readFile path >>= \case
        Nothing -> fileDefault
        Just ft -> do
            syntaxMap <- focusConfig #syntaxMap
            return
                $ maybe (FileContentsPlain $ expandTabs ft)
                        (`FileContentsHL` ft)
                        (SL.lookupSyntax (path ^. #unAbsPath . packed)
                                         syntaxMap)

    defaultRead = do
        maxSize <- focusSettings #maxPreviewSize
        bool readIt fileDefault (fsize > maxSize)

    fileDefault = FileInfo <$> getFileInfo path

    previewImg mt = focusConfig #imageSupport >>= \case
        Nothing -> imgAscii mt
        Just is -> maybeM (imgAscii mt) (return . Img) (drawImage is path)

    imgAscii mt = focusHistory #asciiImgCache <&> (^. at path) >>= \case
        Nothing -> do
            deps <- runMaybeT
                $ (,) <$> lookupDepM "convert" <*> lookupDepM "jp2a"
            case deps of
                Nothing           -> fileDefault
                Just (conv, jp2a) -> convert conv jp2a
        Just t  -> return $ ImgAscii t
      where
        lookupDepM = MaybeT . lookupSoftDep

        convert conv jp2a = do
            txt <- case mt of
                "image/jpeg" -> jp2aCmd $ toFilePath path
                "image/png"  -> withSystemTempFile "norris-jr-ascii-img.jpg"
                    $ \tmpFp _ -> do
                        runProcess_ conv [ toFilePath path, tmpFp ] Nothing
                        jp2aCmd tmpFp
                _            -> return Nothing
            case txt of
                Just t  -> do
                    modifyHistory #asciiImgCache (at path ?~ t)
                    return $ ImgAscii t
                Nothing -> fileDefault
          where
            jp2aCmd fp = runProcessBlocking jp2a [ fp ] Nothing

    previewPdf = lookupSoftDep "pdftotext" >>= \case
        Nothing -> fileDefault
        Just ptt -> maybeM fileDefault (return . FileContentsPlain) cmd
          where
            cmd =
                runProcessBlocking ptt
                                   [ "-nopgbrk", "-q", toFilePath path, "-" ]
                                   Nothing

bulkRename :: ( MonadUnliftIO m
              , MonadProcess m
              , MonadConfig m
              , MonadErr m
              , MonadBrowser m
              , MonadHistory m
              , MonadNav m
              )
           => [Entry]
           -> m ()
bulkRename [] = return ()
bulkRename es = suspendAndRun . withSystemTempFile "norris-jr-rename"
    $ \tmpFp tmpHandle -> do
        liftIO $ T.hPutStr tmpHandle (T.unlines $ entryNames es)
        hClose tmpHandle
        editor <- focusConfig $ #userInfo . #editor
        runInTerm (mkEditorCmd editor tmpFp) >>= \case
            ExitFailure e -> bulkRenameEdFailed editor e
            ExitSuccess   -> mv editor tmpFp
  where
    msgNoRename     = errMessage "No renaming to be done!"

    mv editor tmpFp = do
        bs <- runResourceT . Q.toStrict_ . Q.readFile $ tmpFp
        case T.decodeUtf8' bs of
            Left _    -> bulkRenameDecodeFailed
            Right txt -> do
                let names = T.lines txt
                bool (bulkRenameMv editor es names)
                     msgNoRename
                     (names == entryNames es)

bulkRenameMv
    :: ( MonadErr m
       , MonadProcess m
       , MonadUnliftIO m
       , MonadBrowser m
       , MonadHistory m
       , MonadNav m
       , MonadConfig m
       )
    => Executable
    -> [Entry]
    -> [Text]
    -> m ()
bulkRenameMv editor es names =
    withSystemTempFile "norris-jr-rename" $ \tmpFp tmpHandle -> do
        liftIO $ T.hPutStr tmpHandle mvScript
        hClose tmpHandle
        runInTerm (mkEditorCmd editor tmpFp) >>= \case
            ExitFailure e -> bulkRenameEdFailed editor e
            ExitSuccess   -> moveThem tmpFp
  where
    mvScript = mkMvScript $ zip (entryNames es) names

    mvCmd fp = defaultShCmd $ mconcat [ "/bin/sh", " ", T.pack fp ]

    mkMvScript ns = T.unlines [ header, moveNames ]
      where
        header             =
            T.unlines [ "# This file will be executed after closing"
                      , "# Check it carefully before saving"
                      , "# Clear it to discard all changes"
                      ]

        moveNames          =
            foldr (\(old, new) b -> b
                   <> bool (moveOldNew old new) mempty (old == new)) mempty ns

        moveOldNew old new = mconcat [ "mv -vi -- ", old, " ", new, "\n" ]

    moveThem tmpFp = do
        bs <- runResourceT . Q.toStrict_ . Q.readFile $ tmpFp
        case T.decodeUtf8' bs of
            Left _       -> bulkRenameDecodeFailed
            Right scrTxt -> do
                _ <- catchSh_ runSh (mvCmd tmpFp)
                when (scrTxt == mvScript) $ do
                    cwd <- toFilePath <$> viewCwd
                    for_ (zip es names) $ \(en, n) -> do
                        newAbsPath <- mkAbsPath cwd (T.unpack n)
                        retag (en ^. #item . #path) newAbsPath
                reloadCwd

bulkRenameEdFailed :: MonadErr m => Executable -> Int -> m ()
bulkRenameEdFailed editor exitCode = errMessage
    $ mconcat [ "Bulk rename: "
              , editor ^. to toFilePath . packed
              , " failed with exit code "
              , exitCode ^. to show . packed
              ]

bulkRenameDecodeFailed :: MonadErr m => m ()
bulkRenameDecodeFailed = errMessage
    $ mconcat [ "Bulk rename: "
              , "couldn't decode new names as UTF-8, "
              , "aborting..."
              ]

prependToPath :: MonadNav m => [Char] -> m AbsPath
prependToPath fp = do
    cwd <- toFilePath <$> viewCwd
    final <- loop fp
    mkAbsPath cwd final
  where
    loop p = bool (return p) (loop $ "_" <> p) =<< doesPathExist p

copyFile :: (MonadUnliftIO m, MonadNav m) => PutOptions -> AbsPath -> m ()
copyFile putOpt (AbsPath fp) = do
    cwd <- toFilePath <$> viewCwd
    path <- toFilePath <$> case putOpt of
        Prepend   -> prependToPath relName
        Overwrite -> mkAbsPath cwd relName
    D.copyFileWithMetadata fp path
  where
    relName = T.unpack $ pathName fp

copyDir :: (MonadUnliftIO m, MonadNav m) => PutOptions -> AbsPath -> m ()
copyDir putOpt (AbsPath fp) = do
    cwd <- toFilePath <$> viewCwd
    path <- toFilePath <$> case putOpt of
        Prepend   -> prependToPath relName
        Overwrite -> mkAbsPath cwd relName
    copyDirRecur fp path
  where
    copyDirRecur src dest = isDir dest >>= \case
        True  -> throwIO . userError $ dest <> " already exists"
        False -> do
            _ <- D.createDirectory dest
            ps <- D.listDirectory src
            for_ ps $ \p -> do
                let orig   = src </> p
                    copied = dest </> p
                isD <- isDir orig
                bool (D.copyFileWithMetadata orig copied)
                     (copyDirRecur orig copied)
                     isD

    relName               = T.unpack $ pathName fp

deleteDir :: (MonadUnliftIO m, MonadErr m, MonadBrowser m) => AbsPath -> m ()
deleteDir = deletePath D.removeDirectoryRecursive

deleteFile :: (MonadUnliftIO m, MonadErr m, MonadBrowser m) => AbsPath -> m ()
deleteFile = deletePath D.removeFile

deletePath :: (MonadUnliftIO m, MonadErr m, MonadBrowser m)
           => (FilePath -> m ())
           -> AbsPath
           -> m ()
deletePath del (AbsPath fp) =
    del fp `catchIOErr_` FsError >> modifyEntries B.listMoveDown

dirCount :: MonadUnliftIO m => AbsPath -> m (Maybe Int)
dirCount = handleMaybe (fmap length <$> D.listDirectory) . toFilePath

mkEditorCmd :: Executable -> FilePath -> ShellCmd
mkEditorCmd ed fp =
    defaultShCmd $ mconcat [ ed ^. to toFilePath . packed, " ", T.pack fp ]

getMimeType
    :: (MonadProcess m, MonadConfig m) => AbsPath -> m (Maybe MimeType)
getMimeType (AbsPath fp) = cmd >>= \case
    Nothing -> focusConfig #mimeMap <&> (^. at (mkExtension fp))
    Just mt -> return . Just $ mkMimeType mt
  where
    cmd = do
        file <- getHardDep "file"
        runProcessBlocking file [ "--mime-type", "-Lb", fp ] mempty

getExecutableForMimeType
    :: (MonadProcess m, MonadConfig m) => MimeType -> m (Maybe Executable)
getExecutableForMimeType mt =
    maybe (return Nothing) (searchPath . fromDesktopEntry) =<< cmd
  where
    cmd = do
        xdgMime <- getHardDep "xdg-mime"
        runProcessBlocking xdgMime
                           [ "query"
                           , "default"
                           , mt ^. position @1 . unpacked
                           ]
                           mempty

getImageSize :: (MonadPreview m, MonadProcess m)
             => ImageSupport
             -> AbsPath
             -> m (Maybe DisplayRegion)
getImageSize (ImageSupport W3m prov) path = cmd >>= \case
    Nothing -> return Nothing
    Just size -> case T.words size of
        [ w, h ] -> runMaybeT $ (,) <$> readM w <*> readM h
        _        -> return Nothing
      where
        readM = MaybeT . return . readMaybe . T.unpack
  where
    stdin = "5;" <> toFilePath path

    cmd   = runProcessBlocking (prov ^. #exePath) mempty (Just stdin)
getImageSize (ImageSupport Ueberzug _) _ = return Nothing

drawImage :: (MonadPreview m, MonadProcess m, MonadUI m, MonadUnliftIO m)
          => ImageSupport
          -> AbsPath
          -> m (Maybe ImagePreview)
drawImage w3m@(ImageSupport W3m prov) path   -- FIXME
    = getImageSize w3m path >>= \case
        Nothing    -> return Nothing
        Just isize -> extents <&> (^? _Just . #preview) >>= \case
            Nothing -> return Nothing
            Just ex -> do
                let (startX, startY) = ex ^. to B.extentUpperLeft . B.locL
                    psize            = ex ^. to B.extentSize
                    (w, h)           = resize psize isize
                    ds               = (startX, startY, w, h)
                    cmd              = w3mCmd path ds
                    cleanup          = w3mCleanup ds
                handle
                    <- runWithHandle (P.shell $ prov ^. #exePath & toFilePath)
                                     { P.std_in  = CreatePipe
                                     , P.std_out = CreatePipe
                                     }
                liftIO $ do
                    hPutStr (handle ^. #stdin) cmd
                    hFlush (handle ^. #stdin)
                return . Just $ ImagePreview { .. }
  where
    resize (pw, ph) (iw, ih) =
        let iw' = bool iw pw (iw > pw)
            ih' = bool ih ((ih * pw) `div` iw) (iw > pw)
            w   = bool iw' ((iw' * ph) `div` ih') (ih' > ph)
            h   = bool ih' ph (ih' > ph)
        in
            (w, h)

    w3mCleanup (x, y, w, h) =
        mconcat [ "6;"
                , intercalate [ w3mSep ] $ show <$> [ x, y, w, h ]
                , "\n4;\n3;\n"
                ]

    w3mCmd p (x, y, w, h) =
        mconcat [ "0;1;"
                , intercalate [ w3mSep ] $ show <$> [ x, y, w, h ]
                , replicate 5 w3mSep
                , toFilePath p
                , "\n4;\n3;\n"
                ]

    w3mSep                   = ';'
drawImage (ImageSupport Ueberzug prov) -- FIXME
          path = extents <&> (^? _Just . #preview) >>= \case
    Nothing -> return Nothing
    Just ex -> do
        let (x, y)                = ex ^. to B.extentUpperLeft . B.locL
            (maxWidth, maxHeight) = ex ^. to B.extentSize
            identifier            = "preview"
            uzbgStart             = Object
                $ fromList [ ("action", "add")
                           , ("identifier", identifier)
                           , ("x", num x)
                           , ("y", num y)
                           , ("maxWidth", num maxWidth)
                           , ("maxHeight", num maxHeight)
                           , ("path", String . T.pack $ toFilePath path)
                           ]
            uzbgQuit              = Object
                $ fromList [ ("action", "remove")
                           , ("identifier", identifier)
                           ]
            cmd                   = (bsToStr $ encode uzbgStart) <> "\n"
            cleanup               = bsToStr $ encode uzbgQuit
        handle <- runWithHandle (P.proc (prov ^. #exePath & toFilePath)
                                        [ "layer", "--silent" ])
                                { P.std_in = CreatePipe }

        liftIO $ do
            hPutStr (handle ^. #stdin) (cmd <> "\n")
            hFlush (handle ^. #stdin)
        return . Just $ ImagePreview { .. }
  where
    bsToStr = T.unpack . LT.toStrict . LT.decodeUtf8

    num     = Number . fromIntegral
