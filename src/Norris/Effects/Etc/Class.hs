-- |
module Norris.Effects.Etc.Class
    ( MonadPreview(..)
    , MonadErr(..)
    , MonadResource(..)
    , MonadProcess(..)
    ) where

import           Control.Exception    ( IOException )
import           Control.Monad.Logger ( LogLine )

import           Data.Text            ( Text )

import           Lens.Micro.Platform  ( Lens' )

import           Norris.Types
                 ( AbsPath
                 , Entry
                 , Error
                 , Event
                 , Executable
                 , FInfo
                 , LogChan
                 , Name
                 , Preview
                 , ProcHandle
                 , Resources
                 , ShellCmd
                 )

import           System.Exit          ( ExitCode )
import           System.Process       ( CreateProcess )

import           UnliftIO.Concurrent  ( ThreadId )

class Monad m => MonadPreview m where
    mkPreview :: Entry -> m Preview
    readFile :: AbsPath -> m (Maybe Text)
    getFileInfo :: AbsPath -> m FInfo
    loadNewPreview :: m ()
    getPreview :: m (Maybe Preview)

class Monad m => MonadErr m where
    displayError :: Error -> m ()
    catchIOErr_ :: m () -> (IOException -> Error) -> m ()

class Monad m => MonadResource m where
    focusResource :: Lens' Resources a -> m a
    writeEvChan :: Event -> m ()
    changeFocus :: Name -> m ()
    writeLogChan :: LogLine -> m ()
    runLogWorker :: LogChan LogLine -> m ()

class Monad m => MonadProcess m where
    runProcessBlocking
        :: Executable -> [[Char]] -> Maybe [Char] -> m (Maybe Text)
    runProcess_ :: Executable -> [[Char]] -> Maybe [Char] -> m ()
    runWithHandle :: CreateProcess -> m ProcHandle
    runSh :: ShellCmd -> m ExitCode
    runSh_ :: ShellCmd -> m ()
    runInTerm :: ShellCmd -> m ExitCode
    suspendAndRun :: m () -> m ()
    searchPath :: Text -> m (Maybe Executable)
    fork :: m () -> m ThreadId
