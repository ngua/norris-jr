-- |
module Norris.Effects.UI.Class ( MonadUI(..), MonadConsole(..) ) where

import           Brick            ( BrickEvent )

import           Data.Text        ( Text )
import           Data.Text.Zipper ( TextZipper )

import           Lens.Micro       ( Lens' )

import           Norris.Types
                 ( BindingMode
                 , Command
                 , CommandText
                 , Console
                 , Direction
                 , Event
                 , Extents
                 , Input
                 , InputMode
                 , KeyEvent
                 , Name
                 , Popup
                 , SearchResults
                 , SearchText
                 , Urgency
                 , Viewports
                 )

class Monad m => MonadUI m where
    handleEvent :: BrickEvent Name Event -> m ()
    runCommand :: Command -> m ()
    showPopup :: Popup a -> m ()
    hidePopup :: m ()
    setBindingMode :: BindingMode -> m ()
    getBindingMode :: m BindingMode
    viewports :: m (Maybe Viewports)
    extents :: m (Maybe Extents)
    showBindingPrefix :: KeyEvent -> m ()
    hideBindingPrefix :: m ()
    echo :: Urgency -> Text -> m ()

class Monad m => MonadConsole m where
    getConsoleText :: ([Text] -> a) -> m a
    setConsoleText :: (TextZipper Text -> TextZipper Text) -> m ()
    enterConsole :: InputMode -> m ()
    currentConsole :: m Console
    modifyConsole :: Lens' Console a -> (a -> a) -> m ()
    doSearch :: SearchText -> m ()
    doCommand :: CommandText -> m ()
    currentSearchResults :: m (Maybe SearchResults)
    gotoResult :: Direction -> m ()
    saveInput :: Lens' Console (Input a) -> a -> m ()
