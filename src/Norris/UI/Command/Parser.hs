-- |
module Norris.UI.Command.Parser ( parseCommand, validCommands ) where

import           Control.Applicative  ( Alternative(some, many, (<|>)) )
import           Control.Monad        ( MonadPlus(mzero) )

import           Data.Attoparsec.Text ( Parser )
import qualified Data.Attoparsec.Text as P
import           Data.Bool            ( bool )
import           Data.Foldable        ( asum )
import           Data.Functor         ( ($>) )
import           Data.List.Extra      ( nubOrd )
import qualified Data.Text            as T
import           Data.Text            ( Text )

import           Lens.Micro           hiding ( set )

import           Norris.Types
                 ( Command(..)
                 , CommandLiteral(CmdText, CmdChar)
                 , CommandText(CommandText)
                 , ControlCommand(Echo, Shell, Reload, Quit)
                 , Destination(..)
                 , Error(CommandError)
                 , FileOpCommand(MkDir, Touch)
                 , HighlightStyle(..)
                 , NavCommand(Cd)
                 , ProcessFlag(..)
                 , SettingsCommand(SortDirFirst, SortBy, UseHLStyle, ShowHidden)
                 , ShellCmd(ShellCmd)
                 , SortOrder(..)
                 )

import qualified System.FilePath      as F

parseCommand :: CommandText -> Either Error Command
parseCommand (CommandText t) =
    P.parseOnly commandP t & _Left .~ CommandError t

commandP :: Parser Command
commandP = asum [ Control <$> controlP
                , Navigation <$> navigationP
                , FileOp <$> fileOpP
                , Toggle <$> toggleP
                ]
    <* trailingSpace

controlP :: Parser ControlCommand
controlP = P.char quit <* P.endOfInput $> Quit
    <|> P.string quitTxt $> Quit
    <|> P.char reload <* P.endOfInput $> Reload
    <|> P.string reloadTxt $> Reload
    <|> P.char shell *> shellArg
    <|> P.string shellTxt *> shellArg
    <|> P.string echo *> echoArg
  where
    shellArg = some P.space *> (Shell <$> shellCmdP)

    echoArg  = some P.space *> (Echo <$> P.takeText)

fileOpP :: Parser FileOpCommand
fileOpP = P.string touch *> some P.space *> (Touch <$> filePathP)
    <|> P.string mkdir *> some P.space *> (MkDir <$> filePathP)

toggleP :: Parser SettingsCommand
toggleP =
    asum [ P.string setBang
           *> some P.space
           *> asum [ "show-hidden" $> ShowHidden
                   , "sort dir-first" $> SortDirFirst
                   ]
         , P.string set
           *> some P.space
           *> asum [ "hl-style" *> some P.space *> (UseHLStyle <$> hlStyleP)
                   , "sort" *> some P.space *> (SortBy <$> sortP)
                   ]
         ]

hlStyleP :: Parser HighlightStyle
hlStyleP = "kate" $> Kate
    <|> "breeze-dark" $> BreezeDark
    <|> "pygments" $> Pygments
    <|> "espresso" $> Espresso
    <|> "tango" $> Tango
    <|> "haddock" $> Haddock
    <|> "haddock" $> Haddock
    <|> "monochrome" $> Monochrome
    <|> "zenburn" $> Zenburn

sortP :: Parser SortOrder
sortP = "alph-asc" $> AlphAsc
    <|> "alph-desc" $> AlphDesc
    <|> "size-asc" $> SizeAsc
    <|> "size-desc" $> SizeDesc
    <|> "modified-asc" $> ModifiedAsc
    <|> "modified-desc" $> ModifiedDesc

navigationP :: Parser NavCommand
navigationP = P.string cd *> some P.space *> (Cd <$> locationP)

locationP :: Parser Destination
locationP = P.char home $> Home
    <|> ".." $> Parent
    <|> P.char root $> Root
    <|> Fp <$> filePathP

filePathP :: Parser FilePath
filePathP = do
    fp <- T.unpack <$> P.takeText
    bool mzero (return $ F.normalise fp) (F.isValid fp)

shellCmdP :: Parser ShellCmd
shellCmdP = ShellCmd <$> (nubOrd <$> many (many P.space *> cmdFlagP))
    <*> (many P.space *> P.takeText)

cmdFlagP :: Parser ProcessFlag
cmdFlagP = "-s" $> Fork <|> "-t" $> NeedsTerm

trailingSpace :: Parser ()
trailingSpace = many P.space *> P.endOfInput

validCommands :: [CommandLiteral]
validCommands =
    [ CmdChar quit
    , CmdChar reload
    , CmdChar shell
    , CmdText quitTxt
    , CmdText reloadTxt
    , CmdText shellTxt
    , CmdText mkdir
    , CmdText touch
    , CmdText cd
    , CmdText echo
    , CmdText set
    , CmdText setBang
    ]

quit :: Char
quit = 'q'

reload :: Char
reload = 'r'

shell :: Char
shell = 's'

quitTxt :: Text
quitTxt = "quit"

shellTxt :: Text
shellTxt = "shell"

reloadTxt :: Text
reloadTxt = "reload"

touch :: Text
touch = "touch"

mkdir :: Text
mkdir = "mkdir"

cd :: Text
cd = "cd"

home :: Char
home = '~'

root :: Char
root = '/'

echo :: Text
echo = "echo"

set :: Text
set = "set"

setBang :: Text
setBang = "set!"
