-- |
module Norris.UI.Console
    ( mkScrollback
    , newConsole
    , mkSearchText
    , mkCommandText
    , inputPrompt
    ) where

import           Data.Sequence ( (<|) )
import           Data.Text     ( Text )
import qualified Data.Text     as T

import           Lens.Micro

import qualified Norris.Types
import           Norris.Types
                 ( CommandText(CommandText)
                 , Console(Console)
                 , Input(Input)
                 , InputMode(..)
                 , Scrollable(..)
                 , Scrollback(..)
                 , SearchText(SearchText)
                 , UserPrompt(UserPrompt)
                 )

newConsole :: Console
newConsole = Console
    { inputMode = Nothing
    , search    = Input
          { history       = mempty
          , scrollback    = Nothing
          , tabCompletion = Nothing --
          }
    , command   = Input
          { history       = mempty
          , scrollback    = Nothing
          , tabCompletion = Nothing --
          }
    }

inputPrompt :: InputMode -> Text
inputPrompt SearchPrompt                        = "/"
inputPrompt CommandPrompt                       = ":"
inputPrompt (InputPrompt UserPrompt { prompt }) = prompt

mkSearchText :: [Text] -> SearchText
mkSearchText = SearchText . T.strip . T.concat

mkCommandText :: [Text] -> CommandText
mkCommandText = CommandText . T.strip . T.concat

mkScrollback :: Scrollable a -> Console -> Scrollback a
mkScrollback scrollable Console { search, command } =
    Scrollback { position = 0, items }
  where
    getConsoleText :: Scrollable a -> Text
    getConsoleText (SearchScroll (SearchText t))   = t
    getConsoleText (CommandScroll (CommandText t)) = t

    getSavedText (SearchScroll _)  = search ^. #history <&> (^. #unSearchText )
    getSavedText (CommandScroll _) = command ^. #history <&> (^. #unCommandText )

    items = getConsoleText scrollable <| getSavedText scrollable
