-- |
module Norris.UI.Command
    ( mkBindingCommand
    , describeCommand
    , showBindingHelp
    , mkModeHints
    , defaultBindingHints
    , mkConsoleCommand
    , descMode
    , specialBookmarkChar
    , defaultTag
    ) where

import qualified Brick.Widgets.List as B

import           Data.Char          ( digitToInt, isDigit )
import           Data.Foldable      ( Foldable(foldl') )
import qualified Data.List.NonEmpty as NE
import qualified Data.Map.Strict    as M
import           Data.Maybe         ( mapMaybe )
import           Data.Text          ( Text )
import qualified Data.Text          as T
import           Data.Text.Zipper   ( TextZipper )
import qualified Data.Text.Zipper   as Z

import           Graphics.Vty       ( Event(EvKey), Key(..) )
import           Graphics.Vty.Input ( Modifier(MCtrl) )

import           Lens.Micro

import           Norris.Types
                 ( BindingHints
                 , BindingMode(..)
                 , Command(..)
                 , ConsoleCommand(..)
                 , ControlCommand(..)
                 , DeleteOptions(..)
                 , Destination(..)
                 , Direction(..)
                 , FileOpCommand(..)
                 , InputMode(..)
                 , KeyEvent
                 , ModeHints
                 , NavCommand(..)
                 , Pending(..)
                 , Position(..)
                 , PutOptions(..)
                 , SettingsCommand(..)
                 , SortOrder(..)
                 , Tag(Tag)
                 , Undoable(..)
                 , VisualCommand(..)
                 , YankOptions(..)
                 , pattern EvChar
                 )
import           Norris.Utils       ( showEvChar )

mkBindingCommand :: BindingMode -> KeyEvent -> Maybe Command
mkBindingCommand NormalMode = \case
    EvChar 'V' [] -> Just . Control . ChangeBindingMode $ VisualMode
    EvChar 'g' [] ->
        Just . Control . ChangeBindingMode . PendingMode $ GlobalPrefix
    EvChar 'z' [] ->
        Just . Control . ChangeBindingMode . PendingMode $ SettingsPrefix
    EvChar 'y' [] ->
        Just . Control . ChangeBindingMode . PendingMode $ YankPrefix
    EvChar 'p' [] ->
        Just . Control . ChangeBindingMode . PendingMode $ PutPrefix
    EvChar 'd' [] ->
        Just . Control . ChangeBindingMode . PendingMode $ DeletePrefix
    EvChar 'm' [] ->
        Just . Control . ChangeBindingMode . PendingMode $ BookmarkPrefix
    EvChar '`' [] ->
        Just . Control . ChangeBindingMode . PendingMode $ GotoBookmarkPrefix
    EvChar '\'' [] ->
        Just . Control . ChangeBindingMode . PendingMode $ GotoBookmarkPrefix
    EvChar 'u' [] ->
        Just . Control . ChangeBindingMode . PendingMode $ UndoPrefix Nothing
    EvChar '"' [] ->
        Just . Control . ChangeBindingMode . PendingMode $ TagRegisterPrefix
    EvChar '/' [] -> Just . Control . ConsoleCmd $ EnterConsole SearchPrompt
    EvChar ':' [] -> Just . Control . ConsoleCmd $ EnterConsole CommandPrompt
    EvChar 'q' [] -> Just . Control $ Quit
    EvChar 'r' [] -> Just . Control $ Reload
    EvChar 'n' [] -> Just . Control $ ScrollSearchRes Up
    EvChar 'N' [] -> Just . Control $ ScrollSearchRes Down
    EvChar ' ' [] -> Just . Control $ ToggleMark
    EvChar 't' [] -> Just . Control $ ToggleTag Nothing
    EvChar 'G' [] -> Just . Navigation $ Goto Bottom
    EvKey KEnd [] -> Just . Navigation $ Goto Bottom
    EvChar 'j' [] -> Just . Navigation . Goto $ Line Down
    EvKey KDown [] -> Just . Navigation . Goto $ Line Down
    EvChar 'k' [] -> Just . Navigation . Goto $ Line Up
    EvKey KUp [] -> Just . Navigation . Goto $ Line Up
    EvChar 'd' [ MCtrl ] -> Just . Navigation . Goto $ HalfPage Down
    EvChar 'u' [ MCtrl ] -> Just . Navigation . Goto $ HalfPage Up
    EvChar 'f' [ MCtrl ] -> Just . Navigation . Goto $ Page Down
    EvKey KPageDown [ MCtrl ] -> Just . Navigation . Goto $ Page Down
    EvChar 'b' [ MCtrl ] -> Just . Navigation . Goto $ Page Up
    EvChar 'h' [] -> Just . Navigation $ Cd Parent
    EvChar '[' [] -> Just . Navigation $ Cd PrevChild
    EvChar ']' [] -> Just . Navigation $ Cd NextChild
    EvChar 'R' [] -> Just . FileOp $ Rename Nothing
    EvChar 'B' [] -> Just . FileOp $ BulkRename
    EvChar 'o' [ MCtrl ] -> Just . Navigation $ Cd MostRecent
    EvChar 'l' [] -> Just . FileOp $ Open
    EvKey KEnter [] -> Just . FileOp $ Open
    EvChar 'O' [] -> Just . FileOp $ OpenWith Nothing
    EvChar k []
        | isDigit k -> Just . Control . ChangeBindingMode . PendingMode
            $ LineNumPrefix (NE.fromList [ digitToInt k ])
    _ -> Nothing
mkBindingCommand VisualMode = \case
    EvChar 'j' [] -> Just $ Visual VDown
    EvChar 'k' [] -> Just $ Visual VUp
    EvChar 'g' [] -> Just $ Visual VTop
    EvChar 'G' [] -> Just $ Visual VBottom
    _             -> Nothing

mkBindingCommand (PendingMode GlobalPrefix) = \case
    EvChar '~' []  -> Just . Navigation $ Cd Home
    EvChar '/' []  -> Just . Navigation $ Cd Root
    EvChar 'g' []  -> Just . Navigation $ Goto Top
    EvKey KHome [] -> Just . Navigation $ Goto Top
    EvChar 'm' []  -> Just . Navigation $ Goto Middle
    _              -> Nothing
mkBindingCommand (PendingMode (LineNumPrefix ds)) = \case
    EvChar 'g' [] -> Just . Navigation . Goto $ LineNumber idx
    EvChar 'j' [] -> Just . Navigation . Goto $ LineOffset getOffset
      where
        getOffset browser = getSelected browser >>= \c -> return
            $ min (c + idx) (browser ^. #total)
    EvChar 'k' [] -> Just . Navigation . Goto $ LineOffset getOffset
      where
        getOffset browser = getSelected browser >>= \c -> return
            $ max (c - idx) 0
    EvChar k []
        | isDigit k -> Just . Control . ChangeBindingMode
            $ PendingMode (LineNumPrefix (NE.cons (digitToInt k) ds))
    _ -> Nothing
  where
    getSelected br = br ^. #entries . B.listSelectedL

    idx            = concatDigits $ NE.reverse ds
mkBindingCommand (PendingMode SettingsPrefix) = \case
    EvChar 'h' [] -> Just . Toggle $ ShowHidden
    EvChar 'd' [] -> Just . Toggle $ SortDirFirst
    EvChar 'a' [] -> Just . Toggle $ SortBy AlphAsc
    EvChar 'a' [ MCtrl ] -> Just . Toggle $ SortBy AlphDesc
    EvChar 's' [] -> Just . Toggle $ SortBy SizeAsc
    EvChar 's' [ MCtrl ] -> Just . Toggle $ SortBy SizeDesc
    EvChar 'm' [] -> Just . Toggle $ SortBy ModifiedAsc
    EvChar 'm' [ MCtrl ] -> Just . Toggle $ SortBy ModifiedDesc
    _ -> Nothing
mkBindingCommand (PendingMode YankPrefix) = \case
    EvChar 'y' [] -> Just . FileOp . Yank $ YankEntry
    EvChar 'p' [] -> Just . FileOp . Yank $ YankPathToClip
    _             -> Nothing
mkBindingCommand (PendingMode PutPrefix) = \case
    EvChar 'p' [] -> Just . FileOp . Put $ Prepend
    EvChar 'P' [] -> Just . FileOp . Put $ Overwrite
    _             -> Nothing
mkBindingCommand (PendingMode DeletePrefix) = \case
    EvChar 'd' [] -> Just . FileOp . Delete $ Cut
    EvChar 'D' [] -> Just . FileOp . Delete $ ConsoleDelete Nothing
    _             -> Nothing
mkBindingCommand (PendingMode BookmarkPrefix) = \case
    EvChar k []
        | k /= specialBookmarkChar -> Just . Control $ MakeBookmark k
    _           -> Nothing
mkBindingCommand (PendingMode GotoBookmarkPrefix) = \case
    EvChar k [] -> Just . Navigation . Cd $ Bookmarked k
    _           -> Nothing
mkBindingCommand (PendingMode (UndoPrefix Nothing)) = \case
    EvChar 'm' [] ->
        Just . Control . ChangeBindingMode . PendingMode . UndoPrefix
        $ Just UndoBookmark
    EvChar 't' [] -> Just . Control $ Untag
    EvChar 'M' [] -> Just . Control $ UnmarkAll
    _             -> Nothing
mkBindingCommand (PendingMode (UndoPrefix (Just UndoBookmark))) = \case
    EvChar k []
        | k /= specialBookmarkChar -> Just . Control $ UnBookmark k
    _           -> Nothing
mkBindingCommand (PendingMode TagRegisterPrefix) = \case
    EvChar k [] -> Just . Control $ ToggleTag (Just k)
    _           -> Nothing

specialBookmarkChar :: Char
specialBookmarkChar = '`'

defaultTag :: Tag
defaultTag = Tag '*'

mkConsoleCommand :: InputMode -> KeyEvent -> Command
mkConsoleCommand im@SearchPrompt    = \case
    EvKey KEnter [] -> Control . ConsoleCmd $ SubmitInput im
    EvKey KEsc []   -> Control $ ConsoleCmd LeaveConsole
    EvKey KUp []    -> Control . ConsoleCmd $ ScrollConsoleHist Up im
    EvKey KDown []  -> Control . ConsoleCmd $ ScrollConsoleHist Down im
    EvChar '\t' []  -> Control . ConsoleCmd $ TabComplete im
    evc             -> Control . ConsoleCmd $ ApplyEdit im (getEditOp evc)
mkConsoleCommand im@CommandPrompt   = \case
    EvKey KEnter [] -> Control . ConsoleCmd $ SubmitInput im
    EvKey KEsc []   -> Control $ ConsoleCmd LeaveConsole
    EvChar '\t' []  -> Control . ConsoleCmd $ TabComplete im
    EvKey KUp []    -> Control . ConsoleCmd $ ScrollConsoleHist Up im
    EvKey KDown []  -> Control . ConsoleCmd $ ScrollConsoleHist Down im
    evc             -> Control . ConsoleCmd $ ApplyEdit im (getEditOp evc)
mkConsoleCommand im@(InputPrompt _) = \case
    EvKey KEnter [] -> Control . ConsoleCmd $ SubmitInput im
    EvChar '\t' []  -> Control $ ConsoleCmd LeaveConsole
    EvKey KEsc []   -> Control $ ConsoleCmd LeaveConsole
    evc             -> Control . ConsoleCmd $ ApplyEdit im (getEditOp evc)

getEditOp :: KeyEvent -> (TextZipper Text -> TextZipper Text)
getEditOp = \case
    EvKey KLeft [] -> Z.moveLeft
    EvKey KRight [] -> Z.moveRight
    EvKey KBS [] -> Z.deletePrevChar
    EvKey KHome [] -> Z.gotoBOL
    EvKey KEnd [] -> Z.gotoEOL
    EvChar 'a' [ MCtrl ] -> Z.gotoBOL
    EvChar 'e' [ MCtrl ] -> Z.gotoEOL
    EvChar 'd' [ MCtrl ] -> Z.deleteChar
    EvChar 'k' [ MCtrl ] -> Z.killToEOL
    EvChar 'u' [ MCtrl ] -> Z.killToBOL
    EvChar k []
        | k /= '\t' -> Z.insertChar k
    _ -> id

descMode :: BindingMode -> Text
descMode = \case
    NormalMode -> "Normal"
    VisualMode -> "Visual"
    PendingMode GlobalPrefix -> "Goto..."
    PendingMode SettingsPrefix -> "Toggle settings..."
    PendingMode (LineNumPrefix _) -> "To line no. ..."
    PendingMode YankPrefix -> "Yank..."
    PendingMode PutPrefix -> "Put..."
    PendingMode DeletePrefix -> "Delete..."
    PendingMode BookmarkPrefix -> "Bookmarks..."
    PendingMode GotoBookmarkPrefix -> "Go to bookmark..."
    PendingMode (UndoPrefix Nothing) -> "Undo..."
    PendingMode (UndoPrefix (Just UndoBookmark)) -> "Remove bookmark..."
    PendingMode TagRegisterPrefix -> "Add tag..."

describeCommand :: Command -> Text
describeCommand = \case
    Control Quit -> "Quit"
    Control (ChangeBindingMode bm) -> descMode bm
    Control Reload -> "Reload"
    Control ToggleMark -> "Mark/unmark at point"
    Control UnmarkAll -> "Remove all marks"
    Control Untag -> "Untag"
    Control (ConsoleCmd (EnterConsole im)) -> "Enter " <> case im of
        SearchPrompt  -> "search"
        CommandPrompt -> "command"
        _             -> mempty
    Control (ScrollSearchRes d) -> descDirection d <> " search result"
      where
        descDirection Up   = "Next"
        descDirection Down = "Previous"
    Control _ -> mempty

    Navigation (Cd dest) -> case dest of
        PrevChild  -> "Previous in parent"
        NextChild  -> "Next in parent"
        MostRecent -> "Back in history"
        d          -> "cd " <> case d of
            Home   -> "~"
            Root   -> "/"
            Parent -> ".."
            Fp fp  -> T.pack fp
            _      -> ""
    Navigation (Goto g) -> case g of
        Top          -> "Top"
        Middle       -> "Middle"
        LineNumber _ -> "To line no."
        Bottom       -> "Bottom"
        Line d       -> "Line " <> helpDir d
        HalfPage d   -> "Half-page " <> helpDir d
        Page d       -> "Page " <> helpDir d
        _            -> ""
      where
        helpDir = \case
            Up   -> "up"
            Down -> "down"

    FileOp Open -> "Open"
    FileOp (OpenWith _) -> "Open with"
    FileOp (Yank y) -> "Copy " <> case y of
        YankPathToClip -> "path at point to clipboard"
        _              -> "path at point"
    FileOp (Delete d) -> case d of
        Cut             -> "Cut"
        ConsoleDelete _ -> "Console delete"
    FileOp (Put p) -> "Paste " <> case p of
        Overwrite -> "(overwrite)"
        Prepend   -> "(append)"
    FileOp (Rename _) -> "Rename path at point"
    FileOp BulkRename -> "Bulk rename selection or entry at point"
    FileOp (Touch _) -> "Create empty file"
    FileOp (MkDir _) -> "Create empty directory"

    Toggle ShowHidden -> "Show hidden"
    Toggle SortDirFirst -> "Sort directories first"
    Toggle (SortBy so) -> "Sort " <> case so of
        AlphAsc      -> "alphabetical asc."
        AlphDesc     -> "alphabetical desc."
        SizeAsc      -> "size asc."
        SizeDesc     -> "size desc."
        ModifiedAsc  -> "modification date asc."
        ModifiedDesc -> "modification date desc."

    Toggle _ -> mempty

    Visual _ -> mempty

concatDigits :: (Foldable t, Integral a) => t a -> a
concatDigits = foldl' (\a b -> a * 10 + b) 0

showBindingHelp :: BindingMode -> KeyEvent -> Maybe Text
showBindingHelp bm evc = describeCommand <$> mkBindingCommand bm evc

mkModeHints :: BindingMode -> [KeyEvent] -> ModeHints
mkModeHints bm evs = M.fromList $ mapMaybe hints evs
  where
    hints ev = showBindingHelp bm ev >>= \hint -> return (showEvChar ev, hint)

defaultBindingHints :: BindingHints
defaultBindingHints =
    M.fromList [ normalMHints
               , globalMHints
               , settingsMHints
               , yankMHints
               , delMHints
               , putMHints
               , undoMHints
               , tagRegMHints
               ]
  where
    normalMHints   =
        ( NormalMode
        , mkModeHints NormalMode
                      [ EvChar 'x' []
                      , EvChar 'g' []
                      , EvChar 'G' []
                      , EvChar 'j' []
                      , EvChar 'k' []
                      , EvChar 'd' [ MCtrl ]
                      , EvChar 'u' [ MCtrl ]
                      , EvChar 'f' [ MCtrl ]
                      , EvChar 'b' [ MCtrl ]
                      , EvChar 'z' []
                      , EvChar 'q' []
                      , EvChar 'r' []
                      , EvChar 'h' []
                      , EvChar '[' []
                      , EvChar ']' []
                      , EvChar 'l' []
                      , EvChar 'O' []
                      , EvChar '/' []
                      , EvChar ':' []
                      , EvChar 'n' []
                      , EvChar 'N' []
                      , EvChar 'B' []
                      , EvChar 'o' [ MCtrl ]
                      , EvKey KEnter []
                      ]
        )

    globalMHints   =
        ( PendingMode GlobalPrefix
        , mkModeHints (PendingMode GlobalPrefix)
                      [ EvChar '~' []
                      , EvChar '/' []
                      , EvChar 'g' []
                      , EvChar 'm' []
                      ]
        )

    settingsMHints =
        ( PendingMode SettingsPrefix
        , mkModeHints (PendingMode SettingsPrefix)
                      [ EvChar 'h' []
                      , EvChar 'd' []
                      , EvChar 'a' []
                      , EvChar 'a' [ MCtrl ]
                      , EvChar 's' []
                      , EvChar 's' [ MCtrl ]
                      , EvChar 'm' []
                      , EvChar 'm' [ MCtrl ]
                      ]
        )

    yankMHints     = ( PendingMode YankPrefix
                     , mkModeHints (PendingMode YankPrefix)
                                   [ EvChar 'y' [], EvChar 'p' [] ]
                     )

    delMHints      = ( PendingMode DeletePrefix
                     , mkModeHints (PendingMode DeletePrefix)
                                   [ EvChar 'd' [], EvChar 'D' [] ]
                     )

    putMHints      =
        ( PendingMode PutPrefix
        , mkModeHints (PendingMode PutPrefix) [ EvChar 'p' [], EvChar 'P' [] ]
        )

    undoMHints     =
        ( PendingMode (UndoPrefix Nothing)
        , mkModeHints (PendingMode (UndoPrefix Nothing))
                      [ EvChar 'm' [], EvChar 't' [], EvChar 'M' [] ]
        )

    tagRegMHints   = ( PendingMode TagRegisterPrefix
                     , M.fromList [ ("<any>", "toggle tag at point") ]
                     )
