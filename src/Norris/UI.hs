-- |
module Norris.UI
    ( Name
    , Event(..)
    , handleAppEvent
    , drawUI
    , appStartEvent
    ) where

import           Brick
                 ( (<+>)
                 , (<=>)
                 , BrickEvent(..)
                 , EventM
                 , Next
                 , Widget
                 )
import qualified Brick                     as B
import qualified Brick.Focus               as B
import qualified Brick.Widgets.Center      as B
import qualified Brick.Widgets.Edit        as B
import qualified Brick.Widgets.List        as B
import           Brick.Widgets.List        ( List )
import qualified Brick.Widgets.Skylighting as B

import           Control.Applicative       ( Alternative((<|>)) )
import           Control.Monad.IO.Class    ( MonadIO(liftIO) )
import           Control.Monad.Logger      ( logErrorN )
import           Control.Monad.Trans.Maybe

import           Data.Bool                 ( bool )
import           Data.Maybe                ( fromMaybe )
import qualified Data.Text                 as T

import           Graphics.Vty              ( Modifier(MCtrl) )

import           Lens.Micro

import           Norris.App
                 ( AppEnv
                 , AppState(AppState)
                 , updateAppState
                 )
import qualified Norris.App
import           Norris.AppM               ( AppM(AppM), runAppM )
import           Norris.Effects            ( initSession )
import           Norris.Effects.UI.Class   ( MonadUI(handleEvent) )
import           Norris.FileManager
                 ( abbrevHomeDir
                 , currentEntry
                 , formatFilesize
                 , formatLocalTime
                 , formatPerms
                 , markedEntryIdx
                 , wdMarksTotal
                 )
import           Norris.Types
                 ( BindingMode(..)
                 , Browser
                 , Entry(..)
                 , Event(..)
                 , Extents(Extents)
                 , FileManager
                 , Name(..)
                 , Preview(..)
                 , Tag(Tag)
                 , UserInfo
                 , Viewports(Viewports)
                 , pattern EvChar
                 )
import           Norris.UI.Console         ( inputPrompt )
import           Norris.Utils
                 ( dirAttr
                 , hasMarksAttr
                 , userInfoAttr
                 , workingDirAttr
                 )

import           UnliftIO
                 ( Exception(displayException)
                 , catchIO
                 , throwIO
                 )

appStartEvent :: AppState -> EventM Name AppState
appStartEvent AppState { env } = do
    vps <- mkVPs
    exs <- mkExtents
    liftIO (runWithLog initSession
                       (env & (#viewports .~ vps) . (#extents .~ exs)))
        >> updateAppState env

handleAppEvent
    :: AppState -> BrickEvent Name Event -> EventM Name (Next AppState)
handleAppEvent st@AppState { env } ev = do
    vps <- mkVPs
    exs <- mkExtents
    handleEv (env & (#viewports .~ vps) . (#extents .~ exs)) ev
  where
    handleEv appEnv = \case
        AppEvent QuitNorris -> B.halt st
        AppEvent (SuspendAndResume act) -> do
            B.suspendAndResume
                $ liftIO (runWithLog (AppM act) appEnv)
                >> updateAppState appEnv
        AppEvent (Load c) -> B.continue $ st & #loader ?~ c
        AppEvent Loaded -> do
            st' <- updateAppState appEnv
            B.continue $ st' & #loader .~ Nothing
        VtyEvent (EvChar 'c' [ MCtrl ]) -> B.halt st
        event -> liftIO (runWithLog (handleEvent event) appEnv)
            >> (B.continue =<< updateAppState appEnv)

runWithLog :: AppM a -> AppEnv -> IO a
runWithLog act = runAppM $ act `catchIO` \e -> do
    logErrorN
        $ mconcat [ "norris-jr encountered a fatal error: "
                  , T.pack $ displayException e
                  ]
    throwIO e

mkVPs :: EventM Name (Maybe Viewports)
mkVPs = runMaybeT $ Viewports <$> lookupM BrowserList <*> lookupM PreviewArea
  where
    lookupM = MaybeT . B.lookupViewport

mkExtents :: EventM Name (Maybe Extents)
mkExtents = traverse (return . Extents) =<< B.lookupExtent PreviewArea

drawUI :: AppState -> [Widget Name]
drawUI st = [ B.padAll 1 $ B.vBox [ header, content, echoed, drawFooter ] ]
  where
    header =
        B.withAttr userInfoAttr (st ^. #config . #userInfo & drawUserHost)
        <+> B.withAttr workingDirAttr (B.strWrap path)
        <+> B.txt (st ^. #currentBindingPrefix)
        <+> drawLoader

    drawLoader = case st ^. #loader of
        Nothing -> B.emptyWidget
        Just c  -> B.txt $ T.singleton c

    path = ":" <> st ^. #cwd . to (abbrevHomeDir home) . #unAbsPath

    home = st ^. #config . #userInfo . #homeDir

    content = st ^. #fm & renderFm

    echoed = maybe B.emptyWidget renderEcho (st ^. #echoed)
      where
        renderEcho ech = B.txt $ ech ^. #message

    drawFooter =
        B.padBottom (B.Pad 1) (fromMaybe B.emptyWidget (st ^. #popup))
        <=> drawConsole
        <=> renderFileInfoOrMode st
      where
        drawConsole  = maybe B.emptyWidget
                             (flip (<+>) renderEditor . B.txt . inputPrompt)
                             (st ^. #console . #inputMode)

        renderEditor = B.vLimit 1
            $ B.renderEditor (B.txt . T.unlines)
                             ((st ^. #resources . #focusRing
                               & B.focusGetCurrent)
                              == Just ConsoleEditor)
                             (st ^. #consoleEditor)

renderFm :: FileManager -> Widget Name
renderFm fm = B.hLimitPercent 20 (renderParent fm)
    <+> B.hLimitPercent 30 (fm ^. #browser & renderBrowser)
    <+> renderPreview fm

renderBrowser :: Maybe Browser -> Widget Name
renderBrowser = maybe (B.fill ' ') render
  where
    render br = bool (renderEntries (br ^. #entries))
                     (B.txtWrap "Empty")
                     (br ^. #entries & null)

renderParent :: FileManager -> Widget Name
renderParent fm = renderBrowser (fm ^. #parent)

renderEntries :: List Name Entry -> Widget Name
renderEntries es = B.padRight (B.Pad 1) (B.renderList renderEntry False es)

renderEntry :: Bool -> Entry -> Widget Name
renderEntry _ en = B.forceAttr (entryAttr en)
                               (B.txt hasTag
                                <+> B.txt isMarked
                                <+> B.txtWrap (en ^. #item . #name)
                                <+> B.txt (entrySize en))
  where
    entryAttr (File _) = B.listAttr
    entryAttr (Dir _)  = dirAttr

    entrySize (File f) = f ^. #metadata . #size & formatFilesize
    entrySize (Dir d)  = maybe "?" (T.pack . show) (d ^. #metadata . #count)

    isMarked           = bool mempty " " (en ^. #item . #marked)

    hasTag             = en
        ^. #item . #tag . non (Tag ' ') . #unTag . to T.singleton

renderPreview :: FileManager -> Widget Name
renderPreview fm =
    maybe (B.fill ' ') (B.reportExtent PreviewArea . render) (fm ^. #preview)
  where
    render (DirContents d)        =
        maybe (B.txt "Preview unavailable") renderBrowser (Just d)
    render (FileInfo t)           =
        (B.hCenterWith (Just '-') $ B.txt "FILE INFO")
        <=> B.txtWrap (t ^. #unFInfo)
    render (FileContentsPlain t)
        | T.null t = drawEmpty
        | otherwise = B.padLeftRight 1 (B.txtWrap t)
    render (FileContentsHL syn t)
        | T.null t = drawEmpty
        | otherwise = B.padLeftRight 1 (B.highlight syn t <+> B.fill ' ')
    render (Img _)                = B.emptyWidget
    render (ImgAscii t)           = B.txt t
    render Unavailable            = B.txt "Preview unavailable"

    drawEmpty = B.txt "Empty"

renderFileInfoOrMode :: AppState -> Widget Name
renderFileInfoOrMode AppState { fm, config, bindingMode } =
    maybe (B.txtWrap " ")
          visualOrMetadata
          (fm ^? #browser . _Just . #entries >>= currentEntry)
    <+> ifBrowser totalSize
    <+> ifBrowser count
    <+> ifBrowser marked
  where
    ifBrowser f = maybe B.emptyWidget f (fm ^. #browser)

    visualOrMetadata en = lhs <+> metaData
      where
        lhs      = B.txt $ case bindingMode of
            VisualMode -> "-- VISUAL --  "
            _          -> formatPerms en

        md       = en ^. #item . #metadata

        metaData =
            B.txtWrap $ T.intercalate " " [ filesize, owners, modified ]

        filesize = md ^. #size & formatFilesize

        owners   = md ^. #owner <> ":" <> md ^. #group

        modified = formatLocalTime (md ^. #modified)
                                   (config ^. #userInfo . #timeZone)

    totalSize br = B.txt $ "SUM " <> (br ^. #size & formatFilesize) <> "  "

    count br = B.str $ currentPos br <> "/" <> (br ^. #total & show)

    currentPos br = maybe "?" show pos
      where
        pos = br ^. #entries . B.listSelectedL <&> (+ 1)

    marked br = B.txt " "
        <+> maybe B.emptyWidget
                  (B.withAttr hasMarksAttr . B.str)
                  (getMarked br)

    getMarked br = do
        total <- wdMarksTotal br
        entry <- br ^. #entries & currentEntry
        idx <- (+ 1) <$> markedEntryIdx entry br <|> Just 0
        return $ mconcat [ "MRK", " ", show idx, "/", show total ]

drawUserHost :: UserInfo -> Widget Name
drawUserHost u = B.txt $ (u ^. #username) <> "@" <> (u ^. #host)

