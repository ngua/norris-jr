-- |
module Norris.FileManager
    ( getFileType
    , isHidden
    , epochToLocal
    , formatLocalTime
    , formatPerms
    , formatFilesize
    , emptyBrowser
    , wdSize
    , filterHiddenEntries
    , mkEntryList
    , currentEntry
    , findParentPath
    , recentOrCurrentPath
    , selectChild
    , runSearch
    , isParentOf
    , defaultMimeMap
    , mkMimeType
    , mkExtension
    , fromDesktopEntry
    , abbrevHomeDir
    , wdMarksTotal
    , markedEntries
    , markedEntryIdx
    , entryNames
    , pageSize
    , recPositions
    ) where

import           Brick                 ( Viewport )
import qualified Brick                 as B
import           Brick.Widgets.List    ( List )
import qualified Brick.Widgets.List    as B

import           Data.Bifunctor        ( Bifunctor(bimap) )
import           Data.Bool             ( bool )
import           Data.Function         ( on )
import qualified Data.HashMap.Strict   as HM
import           Data.List             ( elemIndex, isPrefixOf )
import           Data.List.Extra       ( replace )
import qualified Data.Sequence         as Seq
import           Data.Sequence         ( Seq((:<|)) )
import           Data.Text             ( Text )
import qualified Data.Text             as T
import           Data.Time             ( LocalTime, TimeZone )
import qualified Data.Time             as TM
import qualified Data.Time.Clock.POSIX as TM
import           Data.Traversable      ( mapAccumL )
import qualified Data.Vector           as V

import           Lens.Micro.Platform

import qualified Norris.Types
import           Norris.Types
                 ( AbsPath(AbsPath)
                 , Browser(Browser)
                 , Entries
                 , Entry(..)
                 , Extension(Extension)
                 , FileType(..)
                 , IsFilePath(toFilePath, fromFilePath)
                 , MimeMap
                 , MimeType(MimeType)
                 , Name(BrowserList)
                 , Recent
                 , SearchResults(SearchResults)
                 , SearchText(SearchText)
                 , Settings
                 )

import           Numeric               ( showFFloat )

import qualified System.FilePath       as F
import           System.FilePath       ( (</>) )
import           System.Posix
                 ( COff
                 , EpochTime
                 , FileMode
                 , FileOffset
                 , FileStatus
                 )
import qualified System.Posix          as PX

findParentPath :: AbsPath -> Maybe AbsPath
findParentPath (AbsPath fp) =
    bool (Just $ AbsPath parent) Nothing (parent == fp)
  where
    parent = F.takeDirectory fp

wdSize :: Entries -> COff
wdSize = foldr sumSize 0
  where
    sumSize a = (+) (a ^. #item . #metadata . #size)

currentEntry :: List n Entry -> Maybe Entry
currentEntry es = snd <$> B.listSelectedElement es

recentOrCurrentPath :: Recent -> AbsPath -> (AbsPath, Recent)
recentOrCurrentPath Seq.Empty current       = (current, Seq.singleton current)
recentOrCurrentPath (mostRecent :<| rest) _ = (mostRecent, rest)

filterHiddenEntries :: Settings -> Entries -> Entries
filterHiddenEntries ss = V.filter ifHidden
  where
    ifHidden en = not (en ^. #item . #metadata . #isHidden
                       && (ss ^. #showHidden & not))

isParentOf :: IsFilePath a => a -> a -> Bool
isParentOf = isPrefixOf `on` toFilePath

emptyBrowser :: Browser
emptyBrowser = Browser
    { size    = 0
    , total   = 0
    , entries = mkEntryList BrowserList mempty
    , dirHash = 0
    }

mkEntryList :: Name -> Entries -> List Name Entry
mkEntryList n es = B.list n es 1

selectChild :: AbsPath -> Browser -> Browser
selectChild path = #entries
    %~ B.listFindBy (\en -> en ^. #item . #path == path)

runSearch :: Bool -> SearchText -> Entries -> Int -> SearchResults
runSearch icase searchText@(SearchText txt) es idx =
    SearchResults { searchText, results, position = 0 }
  where
    results = Seq.fromList . searchEntries icase $ rest <> h
      where
        (h, rest) = V.splitAt idx es

    searchEntries ic = (^.. each . filtered (isPrefix ic txt))

    isPrefix ic t en = en ^. #item . #name
        & shouldLower ic
        & T.isPrefixOf (t & shouldLower ic)

    shouldLower = bool id T.toLower

getFileType :: FileStatus -> FileType
getFileType fs
    | PX.isDirectory fs = Directory
    | PX.isRegularFile fs = RegularFile
    | PX.isSymbolicLink fs = SymLink
    | PX.isBlockDevice fs = BlockDevice
    | PX.isCharacterDevice fs = CharacterDevice
    | PX.isSocket fs = Socket
    | PX.isNamedPipe fs = NamedPipe
    | otherwise = RegularFile

isHidden :: IsFilePath a => a -> Bool
isHidden fp = "." `isPrefixOf` (last . F.splitPath $ toFilePath fp)

formatLocalTime :: EpochTime -> TimeZone -> Text
formatLocalTime et tz = T.pack
    . TM.formatTime TM.defaultTimeLocale "%Y %b %d %H:%M"
    $ epochToLocal et tz

formatPerms :: Entry -> Text
formatPerms en = withDirPrefix en
    <> (en ^. #item . #metadata . #mode & allPerms)
  where
    withDirPrefix (Dir _)  = "d"
    withDirPrefix (File _) = "-"

formatFilesize :: FileOffset -> Text
formatFilesize = getSize . fromIntegral
  where
    getSize n
        | n < kib = (T.pack $ show n) <> "B"
        | n < mib = showTrunc n kib "K"
        | n < gib = showTrunc n mib "M"
        | n < tib = showTrunc n gib "G"
        | otherwise = showTrunc n tib "T"

    kib             = 1024 :: Int

    mib             = kib ^ (2 :: Int)

    gib             = kib ^ (3 :: Int)

    tib             = kib ^ (4 :: Int)

    coerceFDiv      = (/) `on` fromIntegral

    showTrunc x y f = T.pack $ showFFloat @Float (Just 1) (coerceFDiv x y) f

allPerms :: FileMode -> Text
allPerms fm = mconcat $ [ ownerPerms, groupPerms, otherPerms ] <*> [ fm ]

epochToLocal :: EpochTime -> TimeZone -> LocalTime
epochToLocal et tz =
    TM.utcToLocalTime tz (TM.posixSecondsToUTCTime $ realToFrac et)

ownerPerms :: FileMode -> Text
ownerPerms fm = T.pack
    $ [ showReadPerm PX.ownerReadMode
      , showWritePerm PX.ownerWriteMode
      , showExPerm PX.ownerExecuteMode
      ]
    <*> [ fm ]

groupPerms :: FileMode -> Text
groupPerms fm = T.pack
    $ [ showReadPerm PX.groupReadMode
      , showWritePerm PX.groupWriteMode
      , showExPerm PX.groupExecuteMode
      ]
    <*> [ fm ]

otherPerms :: FileMode -> Text
otherPerms fm = T.pack
    $ [ showReadPerm PX.otherReadMode
      , showWritePerm PX.otherWriteMode
      , showExPerm PX.otherExecuteMode
      ]
    <*> [ fm ]

showPerm :: Char -> FileMode -> FileMode -> Char
showPerm c x y = bool '-' c $ hasPerm x y

showReadPerm :: FileMode -> FileMode -> Char
showReadPerm = showPerm 'r'

showWritePerm :: FileMode -> FileMode -> Char
showWritePerm = showPerm 'w'

showExPerm :: FileMode -> FileMode -> Char
showExPerm = showPerm 'x'

hasPerm :: FileMode -> FileMode -> Bool
hasPerm x y = PX.intersectFileModes y x == x

mkMimeType :: Text -> MimeType
mkMimeType = MimeType . T.stripEnd

mkExtension :: IsFilePath a => a -> Extension
mkExtension = Extension . T.pack . F.takeExtension . toFilePath

fromDesktopEntry :: Text -> Text
fromDesktopEntry = T.takeWhile (F.extSeparator /=)

abbrevHomeDir :: AbsPath -> AbsPath -> AbsPath
abbrevHomeDir home p = fromFilePath
    $ replace (toFilePath home) "~" (toFilePath p)

wdMarksTotal :: Browser -> Maybe Int
wdMarksTotal br = bool Nothing (Just markedCount) (markedCount > 0)
  where
    markedCount = length $ markedEntries br

markedEntryIdx :: Entry -> Browser -> Maybe Int
markedEntryIdx en br = en `elemIndex` markedEntries br

markedEntries :: Browser -> [Entry]
markedEntries br = br ^.. #entries . traversed . filtered (^. #item . #marked)

entryNames :: Traversable t => t Entry -> [Text]
entryNames es = es ^.. traversed . (#item . #name)

pageSize :: Viewport -> List Name Entry -> Int
pageSize v es = v ^. B.vpSize . _2 `div` es ^. B.listItemHeightL

recPositions :: AbsPath -> [(AbsPath, AbsPath)]
recPositions (AbsPath fp) =
    case F.dropTrailingPathSeparator <$> F.splitPath fp of
        [] -> []
        ps -> bimap fromFilePath fromFilePath <$> absPaths
          where
            (_, !absPaths) = mapAccumL mkAbs mempty paths

            mkAbs acc (parent, child) = (newAcc, (newAcc, newChild))
              where
                !newAcc  = acc </> parent

                newChild = newAcc </> child

            paths = zip <*> tail $ ps

defaultMimeMap :: MimeMap
defaultMimeMap =
    HM.fromList [ ("a", "application/octet-stream")
                , ("ai", "application/postscript")
                , ("aif", "audio/x-aiff")
                , ("aifc", "audio/x-aiff")
                , ("aiff", "audio/x-aiff")
                , ("au", "audio/basic")
                , ("avi", "video/x-msvideo")
                , ("bat", "text/plain")
                , ("bcpio", "application/x-bcpio")
                , ("bin", "application/octet-stream")
                , ("bmp", "image/x-ms-bmp")
                , ("cdf", "application/x-netcdf")
                , ("cpio", "application/x-cpio")
                , ("csh", "application/x-csh")
                , ("css", "text/css")
                , ("dll", "application/octet-stream")
                , ("doc", "application/msword")
                , ("dot", "application/msword")
                , ("dvi", "application/x-dvi")
                , ("eml", "message/rfc822")
                , ("eps", "application/postscript")
                , ("etx", "text/x-setext")
                , ("exe", "application/octet-stream")
                , ("gif", "image/gif")
                , ("gtar", "application/x-gtar")
                , ("h", "text/plain")
                , ("hdf", "application/x-hdf")
                , ("htm", "text/html")
                , ("html", "text/html")
                , ("ief", "image/ief")
                , ("jpe", "image/jpeg")
                , ("jpeg", "image/jpeg")
                , ("jpg", "image/jpeg")
                , ("ksh", "text/plain")
                , ("latex", "application/x-latex")
                , ("m1v", "video/mpeg")
                , ("man", "application/x-troff-man")
                , ("me", "application/x-troff-me")
                , ("mht", "message/rfc822")
                , ("mhtml", "message/rfc822")
                , ("mif", "application/x-mif")
                , ("mov", "video/quicktime")
                , ("movie", "video/x-sgi-movie")
                , ("mp2", "audio/mpeg")
                , ("mp3", "audio/mpeg")
                , ("mpa", "video/mpeg")
                , ("mpe", "video/mpeg")
                , ("mpeg", "video/mpeg")
                , ("mpg", "video/mpeg")
                , ("ms", "application/x-troff-ms")
                , ("nc", "application/x-netcdf")
                , ("nws", "message/rfc822")
                , ("o", "application/octet-stream")
                , ("obj", "application/octet-stream")
                , ("oda", "application/oda")
                , ("p12", "application/x-pkcs12")
                , ("p7c", "application/pkcs7-mime")
                , ("pbm", "image/x-portable-bitmap")
                , ("pdf", "application/pdf")
                , ("pfx", "application/x-pkcs12")
                , ("pgm", "image/x-portable-graymap")
                , ("png", "image/png")
                , ("pnm", "image/x-portable-anymap")
                , ("pot", "application/vnd.ms-powerpoint")
                , ("ppa", "application/vnd.ms-powerpoint")
                , ("ppm", "image/x-portable-pixmap")
                , ("pps", "application/vnd.ms-powerpoint")
                , ("ppt", "application/vnd.ms-powerpoint")
                , ("ps", "application/postscript")
                , ("pwz", "application/vnd.ms-powerpoint")
                , ("ra", "audio/x-pn-realaudio")
                , ("ram", "application/x-pn-realaudio")
                , ("ras", "image/x-cmu-raster")
                , ("rdf", "application/xml")
                , ("rgb", "image/x-rgb")
                , ("roff", "application/x-troff")
                , ("rtx", "text/richtext")
                , ("sgm", "text/x-sgml")
                , ("sgml", "text/x-sgml")
                , ("sh", "application/x-sh")
                , ("shar", "application/x-shar")
                , ("snd", "audio/basic")
                , ("so", "application/octet-stream")
                , ("src", "application/x-wais-source")
                , ("sv4cpio", "application/x-sv4cpio")
                , ("sv4crc", "application/x-sv4crc")
                , ("swf", "application/x-shockwave-flash")
                , ("t", "application/x-troff")
                , ("tar", "application/x-tar")
                , ("tcl", "application/x-tcl")
                , ("tex", "application/x-tex")
                , ("texi", "application/x-texinfo")
                , ("texinfo", "application/x-texinfo")
                , ("tif", "image/tiff")
                , ("tiff", "image/tiff")
                , ("tr", "application/x-troff")
                , ("tsv", "text/tab-separated-values")
                , ("txt", "text/plain")
                , ("ustar", "application/x-ustar")
                , ("vcf", "text/x-vcard")
                , ("wav", "audio/x-wav")
                , ("wiz", "application/msword")
                , ("xbm", "image/x-xbitmap")
                , ("xlb", "application/vnd.ms-excel")
                , ("xls", "application/vnd.ms-excel")
                , ("xml", "text/xml")
                , ("xpm", "image/x-xpixmap")
                , ("xsl", "application/xml")
                , ("xwd", "image/x-xwindowdump")
                , ("zip", "application/zip")
                ]
