{-# LANGUAGE ViewPatterns #-}

-- |
module Norris.Config ( XdgData(..), sortEntries, toSlStyle, mkBookmark ) where

import           Control.Applicative          ( Alternative((<|>)) )

import qualified Data.Attoparsec.Text         as P
import           Data.Attoparsec.Text         ( Parser )
import           Data.Char                    ( isDigit, isUpper )
import           Data.Function                ( on )
import qualified Data.Map.Strict              as M
import           Data.Maybe                   ( mapMaybe )
import           Data.Ord                     ( comparing )
import qualified Data.Sequence                as Seq
import           Data.Text                    ( Text )
import qualified Data.Text                    as T
import qualified Data.Vector                  as V
import qualified Data.Vector.Algorithms.Intro as V

import           Lens.Micro.Platform

import           Norris.Types
import           Norris.UI.Command            ( defaultTag )

import           Skylighting                  ( Style )
import qualified Skylighting                  as SL

import           Text.Read                    ( readMaybe )

class XdgData a where
    toXdgData :: a -> Text
    fromXdgData :: Text -> a

instance XdgData Bookmarks where
    toXdgData = M.foldrWithKey' foldBms mempty . M.filter (^. #persistent)
      where
        foldBms k v a = mconcat [ T.singleton k
                                , ":"
                                , v ^. #path . #unAbsPath . packed
                                , "\n"
                                , a
                                ]

    fromXdgData = M.fromList
        . fmap (\bm -> (bm ^. #char, bm))
        . mapMaybe parseBookmark
        . T.words

instance XdgData Tags where
    toXdgData = M.foldrWithKey' foldTgs mempty
      where
        foldTgs k v a =
            mconcat [ k ^. #unAbsPath . packed, showTag v, "\n", a ]

        showTag (Tag '*') = mempty
        showTag (Tag t)   = ":" <> T.singleton t

    fromXdgData = M.fromList . mapMaybe parseTag . T.words

instance XdgData (Input SearchText) where
    toXdgData = concatHistory #unSearchText T.unlines

    fromXdgData = mkInput SearchText

instance XdgData (Input CommandText) where
    toXdgData = concatHistory #unCommandText T.unlines

    fromXdgData = mkInput CommandText

concatHistory :: Lens' a b -> ([b] -> c) -> Input a -> c
concatHistory unwrap merge inp = inp ^.. #history . each . unwrap & merge

mkInput :: (Text -> a) -> Text -> Input a
mkInput g ts = Input (Seq.fromList $ g <$> T.lines ts) Nothing Nothing

sortEntries :: Settings -> Entries -> Entries
sortEntries Settings { sortOrder, sortDirFirst } = V.modify
    $ V.sortBy (sortBy sortOrder)
  where
    sortBy AlphAsc      = sortNatEs sortDirFirst False
    sortBy AlphDesc     = sortEs sortDirFirst getName True
    sortBy SizeAsc      = sortEs sortDirFirst getSize False
    sortBy SizeDesc     = sortEs sortDirFirst getSize True
    sortBy ModifiedAsc  = sortEs sortDirFirst getModified False
    sortBy ModifiedDesc = sortEs sortDirFirst getModified True

    sortNatEs False shouldFlip en1 = cmpBoth
      where
        cmpBoth en2
            | shouldFlip = (natSort `on` getName) en2 en1
            | otherwise = (natSort `on` getName) en1 en2

    sortNatEs True shouldFlip d1@(Dir _) = cmpDir
      where
        cmpDir d2@(Dir _)
            | shouldFlip = (natSort `on` getName) d2 d1
            | otherwise = (natSort `on` getName) d1 d2
        cmpDir (File _)   = LT

    sortNatEs True shouldFlip f1@(File _) = cmpFile
      where
        cmpFile f2@(File _)
            | shouldFlip = (natSort `on` getName) f2 f1
            | otherwise = (natSort `on` getName) f1 f2
        cmpFile (Dir _)     = GT

    sortEs False field shouldFlip en1 = cmpBoth
      where
        cmpBoth en2
            | shouldFlip = comparing field en2 en1
            | otherwise = comparing field en1 en2

    sortEs True field shouldFlip d1@(Dir _) = cmpDir
      where
        cmpDir d2@(Dir _)
            | shouldFlip = comparing field d2 d1
            | otherwise = comparing field d1 d2
        cmpDir (File _)   = LT

    sortEs True field shouldFlip f1@(File _) = cmpFile
      where
        cmpFile f2@(File _)
            | shouldFlip = comparing field f2 f1
            | otherwise = comparing field f1 f2
        cmpFile (Dir _)     = GT

    getSize = (^. #item . #metadata . #size)

    getModified = (^. #item . #metadata . #modified)

    getName = (^. #item . #name)

natSort :: Text -> Text -> Ordering
natSort = comparing natSortKey

natSortKey :: Text -> NatSortKey
natSortKey = NatSortKey . chunks
  where
    chunks (T.null -> True) = mempty
    chunks (T.uncons -> Nothing) = mempty
    chunks t@(T.uncons -> Just _) = case num ^. unpacked & readMaybe of
        Just n  -> Numeric n (T.length num) : chunks afterNum
        Nothing -> Textual (T.zip (T.toLower txt) txt) : chunks afterTxt
      where
        (num, afterNum) = T.span isDigit t

        (txt, afterTxt) = T.break isDigit t
    chunks _ = mempty

toSlStyle :: HighlightStyle -> Style
toSlStyle = \case
    Kate       -> SL.kate
    BreezeDark -> SL.breezeDark
    Pygments   -> SL.pygments
    Espresso   -> SL.espresso
    Tango      -> SL.tango
    Haddock    -> SL.haddock
    Monochrome -> SL.monochrome
    Zenburn    -> SL.zenburn

mkBookmark :: Char -> AbsPath -> Bookmark
mkBookmark char path = Bookmark { .. }
  where
    persistent = isUpper char

xdgDataSep :: Char
xdgDataSep = ':'

parseOrNothing :: Parser a -> Text -> Maybe a
parseOrNothing p txt =
    either (const Nothing) Just (P.parseOnly (p <* P.endOfInput) txt)

parseBookmark :: Text -> Maybe Bookmark
parseBookmark = parseOrNothing bookmarkP

bookmarkP :: Parser Bookmark
bookmarkP = Bookmark <$> (P.anyChar <* P.char xdgDataSep)
    <*> (AbsPath . T.unpack <$> P.takeText)
    <*> pure True

parseTag :: Text -> Maybe (AbsPath, Tag)
parseTag = parseOrNothing tagP

tagP :: Parser (AbsPath, Tag)
tagP = (,) <$> (AbsPath . T.unpack <$> P.takeTill (xdgDataSep ==))
    <*> ((P.char xdgDataSep *> (Tag <$> P.anyChar)) <|> return defaultTag)

