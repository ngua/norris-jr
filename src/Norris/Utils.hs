-- |
module Norris.Utils
    ( checkProcessOutput
    , expandTabs
    , quote
    , showErr
    , emptyCache
    , cacheTrim
    , cacheInsert
    , cacheLookup
    , cacheDelete
    , cacheToList
    , emptyHistory
    , formatLog
    , dirAttr
    , userInfoAttr
    , workingDirAttr
    , defaultTheme
    , updateSyntaxAttrs
    , showEvChar
    , searchEntries
    , scrollWithBounds
    , scrollWrapped
    , hasMarksAttr
    , defaultShCmd
    , exeToCmd
    ) where

import qualified Brick                     as B
import           Brick                     ( AttrMap, AttrName )
import           Brick.Themes              ( Theme )
import qualified Brick.Themes              as B
import qualified Brick.Widgets.List        as B
import qualified Brick.Widgets.Skylighting as B

import           Control.Exception         ( Exception(displayException) )
import           Control.Monad.Logger
                 ( LogLevel(..)
                 , LogLine
                 , fromLogStr
                 )

import           Data.Bool                 ( bool )
import           Data.ByteString.Char8     ( ByteString )
import qualified Data.ByteString.Char8     as C
import qualified Data.HashPSQ              as PS
import           Data.Hashable             ( Hashable )
import           Data.String               ( IsString )
import           Data.Text                 ( Text )
import qualified Data.Text                 as T
import qualified Data.Text.Encoding        as T

import qualified Graphics.Vty              as Vty
import           Graphics.Vty              ( Modifier(..) )

import           Lens.Micro.Platform

import qualified Norris.Types
import           Norris.Types
                 ( Cache(Cache)
                 , Capacity
                 , Direction(..)
                 , Entries
                 , Entry
                 , Error(..)
                 , Executable
                 , History(History)
                 , IsFilePath(toFilePath)
                 , KeyEvent
                 , Priority
                 , SearchText(SearchText)
                 , ShellCmd(ShellCmd)
                 , pattern EvChar
                 )

import           Skylighting               ( Style )

import           System.Exit               ( ExitCode(ExitSuccess) )

checkProcessOutput :: ExitCode -> [Char] -> Maybe Text
checkProcessOutput exitCode out
    | exitCode /= ExitSuccess = Nothing
    | null out = Nothing
    | otherwise = Just $ T.pack out

expandTabs :: Text -> Text
expandTabs = T.replace "\t" (T.replicate 8 " ")

quote :: (IsString a, Monoid a) => a -> a
quote s = "\"" <> s <> "\""

showErr :: Error -> Text
showErr = \case
    FsError err      -> T.pack $ displayException err
    MiscError t      -> t
    CommandError t   -> "Command not found: " <> quote t
    ProcessError err -> T.pack $ displayException err

emptyCache :: Capacity Int -> Cache k v
emptyCache cap = Cache { tick = 0, size = 0, queue = PS.empty, cap }

cacheTrim :: (Ord k, Hashable k) => Cache k v -> Cache k v
cacheTrim cache
    | cache ^. #tick == maxBound = emptyCache (cache ^. #cap)
    | cache ^. #size > (cache ^. #cap . to fromIntegral) = cache
        & (#size -~ 1) . (#queue %~ PS.deleteMin)
    | otherwise = cache

cacheInsert :: (Ord k, Hashable k) => k -> v -> Cache k v -> Cache k v
cacheInsert k v cache = cacheTrim
    $! cache
    & (#tick +~ 1)
    . (#size & bool (+~ 1) (+~ 0) (has _Just oldValM))
    . (#queue .~ queue)
  where
    (oldValM, queue) = PS.insertView k (cache ^. #tick) v (cache ^. #queue)

cacheLookup :: (Ord k, Hashable k) => k -> Cache k v -> Maybe (v, Cache k v)
cacheLookup k cache = case PS.alter bumpPriority k (cache ^. #queue) of
    (Nothing, _) -> Nothing
    (Just x, queue) -> Just (x, newCache)
      where
        !newCache = cacheTrim $! cache & (#tick +~ 1) . (#queue .~ queue)
  where
    bumpPriority Nothing       = (Nothing, Nothing)
    bumpPriority (Just (_, x)) = (Just x, Just (cache ^. #tick, x))

cacheDelete :: (Ord k, Hashable k) => k -> Cache k v -> Cache k v
cacheDelete k cache = case PS.deleteView k (cache ^. #queue) of
    Nothing -> cache
    Just (_, _, queue) -> newCache
      where
        !newCache = cacheTrim $! cache & (#size -~ 1) . (#queue .~ queue)

cacheToList :: (Ord k, Hashable k) => Cache k v -> [(k, Priority, v)]
cacheToList = (^. #queue . to PS.toList)

emptyHistory :: Capacity Int -> History
emptyHistory cap = History
    { recent         = mempty
    , usernameCache  = mempty
    , groupnameCache = mempty
    , browserCache   = emptyCache cap
    , savedPositions = mempty
    , asciiImgCache  = mempty
    }

formatLog :: LogLine -> ByteString
formatLog (_, src, lvl, str) =
    C.unwords [ T.encodeUtf8 src, showLevel lvl, fromLogStr str <> "\n" ]
  where
    showLevel = \case
        LevelDebug     -> "DEBUG"
        LevelInfo      -> "INFO"
        LevelWarn      -> "WARN"
        LevelError     -> "ERROR"
        LevelOther txt -> T.encodeUtf8 txt

defaultTheme :: Theme
defaultTheme =
    B.newTheme defaultAttr
               [ (dirAttr, defaultAttr `Vty.withStyle` Vty.bold)
               , (userInfoAttr, B.fg Vty.brightBlue `Vty.withStyle` Vty.bold)
               , (workingDirAttr, B.fg Vty.brightBlue)
               , ( B.listSelectedAttr
                 , defaultAttr `Vty.withStyle` Vty.reverseVideo
                 )
               , (hasMarksAttr, defaultAttr `Vty.withStyle` Vty.reverseVideo)
               ]
  where
    defaultAttr = B.fg Vty.white

dirAttr :: AttrName
dirAttr = B.attrName "dirAttr"

userInfoAttr :: AttrName
userInfoAttr = B.attrName "userInfoAttr"

workingDirAttr :: AttrName
workingDirAttr = B.attrName "workingDirAttr"

hasMarksAttr :: AttrName
hasMarksAttr = B.attrName "hasMarksAttr"

updateSyntaxAttrs :: Style -> AttrMap -> AttrMap
updateSyntaxAttrs = B.applyAttrMappings . B.attrMappingsForStyle

showEvChar :: KeyEvent -> Text
showEvChar (EvChar k ms) = showMods <> sep <> T.singleton k
  where
    showMods = T.intercalate "-" $ showModifier <$> ms

    sep      = bool "-" mempty (T.null showMods)
showEvChar _ = mempty

showModifier :: Modifier -> Text
showModifier = \case
    MCtrl  -> "C"
    MShift -> "S"
    MAlt   -> "M"
    _      -> ""

searchEntries :: SearchText -> Entries -> [Entry]
searchEntries (SearchText t) es = es ^.. each . filtered prefix
  where
    prefix en = T.isPrefixOf t (en ^. #item . #name)

scrollWithBounds :: Foldable t => Direction -> t a -> Int -> Int
scrollWithBounds Up xs x  = min (length xs) (x + 1)
scrollWithBounds Down _ x = max 0 (x - 1)

scrollWrapped :: Foldable t => Direction -> t a -> Int -> Int
scrollWrapped Up xs x   =
    let next = x + 1 in bool next 0 (next > length xs - 1)
scrollWrapped Down xs x =
    let next = x - 1 in bool next (length xs - 1) (next < 0)

defaultShCmd :: Text -> ShellCmd
defaultShCmd cmd = ShellCmd { .. }
  where
    flags = mempty

exeToCmd :: Executable -> Text -> ShellCmd
exeToCmd exe arg = ShellCmd { .. }
  where
    flags = exe ^. #defaultFlags

    cmd   = mconcat [ exe ^. #exePath . to toFilePath . packed, " ", arg ]
