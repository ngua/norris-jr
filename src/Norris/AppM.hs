-- |
module Norris.AppM ( AppM(AppM), runAppM ) where

import qualified Brick                            as B
import           Brick
                 ( (<=>)
                 , BrickEvent(VtyEvent, AppEvent)
                 )
import qualified Brick.BChan                      as B
import qualified Brick.Focus                      as B
import qualified Brick.Widgets.Border             as B
import qualified Brick.Widgets.Edit               as B
import qualified Brick.Widgets.List               as B

import           Control.Monad                    ( forever, void, when )
import           Control.Monad.Extra              ( maybeM )
import           Control.Monad.Logger             ( MonadLogger(..) )
import qualified Control.Monad.Logger             as ML
import           Control.Monad.Reader
                 ( MonadIO(..)
                 , MonadReader
                 , ReaderT(..)
                 )

import           Data.Bool                        ( bool )
import qualified Data.ByteString.Char8            as C
import           Data.Foldable                    ( for_ )
import           Data.Foldable.Extra              ( traverse_ )
import           Data.Function                    ( on )
import           Data.Generics.Product.Positions  ( HasPosition(position) )
import           Data.Maybe                       ( catMaybes, fromMaybe )
import           Data.Sequence                    ( (<|) )
import qualified Data.Text                        as T
import qualified Data.Text.Encoding               as T
import qualified Data.Text.Lazy                   as LT
import           Data.Text.Lazy.Encoding          ( decodeUtf8' )
import qualified Data.Time                        as TM
import qualified Data.Time.Format.ISO8601         as TM
import           Data.Traversable                 ( for )
import qualified Data.Vector                      as V

import           GHC.Generics                     ( Generic )

import           Lens.Micro.Platform              hiding ( preview )

import           Norris.App                       ( AppEnv )
import           Norris.Config
                 ( XdgData(toXdgData, fromXdgData)
                 , mkBookmark
                 )
import           Norris.Effects
                 ( catchSh_
                 , cmpCached
                 , consoleEvent
                 , copyDir
                 , copyFile
                 , currentBrowser
                 , deleteDir
                 , deleteFile
                 , errMessage
                 , fmEvent
                 , getExecutableForMimeType
                 , getHardDep
                 , getMimeType
                 , leaveConsole
                 , leaveCwd
                 , lookupHardDep
                 , lookupOwners
                 , mkEntry
                 , modifyEntries
                 , modify_
                 , newBrowser
                 , newPreview
                 , reactFSChange
                 , retag
                 , runControlCommand
                 , runFileOpCommand
                 , runNavCommand
                 , runSettingsComand
                 , runVisualCommand
                 , setBrowser
                 , setParent
                 , showLoader
                 , swap_
                 , xdgFile
                 )
import           Norris.Effects.Common            ( getCurrentIdx )
import           Norris.Effects.Config.Class      ( MonadConfig(..) )
import           Norris.Effects.Etc.Class
                 ( MonadErr(..)
                 , MonadPreview(..)
                 , MonadProcess(..)
                 , MonadResource(..)
                 )
import           Norris.Effects.FileManager.Class
                 ( MonadBrowser(..)
                 , MonadFile(..)
                 , MonadHistory(..)
                 , MonadNav(..)
                 )
import           Norris.Effects.UI.Class
                 ( MonadConsole(..)
                 , MonadUI(..)
                 )
import           Norris.FileManager
                 ( currentEntry
                 , findParentPath
                 , mkExtension
                 , recPositions
                 , recentOrCurrentPath
                 , runSearch
                 , selectChild
                 )
import qualified Norris.Types
import           Norris.Types
                 ( AbsPath(AbsPath)
                 , Child(ChildTree, Child)
                 , Command(..)
                 , DirTree(DirTree)
                 , Echoed(Echoed)
                 , Entry(Dir, File)
                 , Error(..)
                 , Event(..)
                 , FInfo(FInfo)
                 , IsFilePath(fromFilePath, toFilePath)
                 , LogChan(LogChan)
                 , Name(..)
                 , Owner(..)
                 , Preview(Unavailable)
                 , ProcHandle(ProcHandle)
                 , ProcessFlag(..)
                 , Urgency(Error)
                 )
import           Norris.UI.Command.Parser         ( parseCommand )
import           Norris.Utils
                 ( cacheDelete
                 , cacheInsert
                 , checkProcessOutput
                 , exeToCmd
                 , formatLog
                 , quote
                 , scrollWrapped
                 , showErr
                 , showEvChar
                 )

import qualified Streaming.ByteString.Char8       as Q
import qualified Streaming.Prelude                as S

import qualified System.FilePath                  as F
import           System.FilePath                  ( (</>) )
import qualified System.Posix                     as PX
import qualified System.Process                   as P ( system )

import           UnliftIO
                 ( MonadUnliftIO
                 , atomically
                 , catchIO
                 , handle
                 , handleIO
                 , isEmptyMVar
                 , putMVar
                 , race_
                 , readMVar
                 , readTBQueue
                 , stderr
                 , takeMVar
                 , throwIO
                 , tryIO
                 , tryTakeMVar
                 , writeTBQueue
                 )
import           UnliftIO.Concurrent
                 ( forkIO
                 , killThread
                 , threadDelay
                 )
import qualified UnliftIO.Directory               as D
import qualified UnliftIO.Process                 as P
import           UnliftIO.Resource                ( runResourceT )

newtype AppM a = AppM (ReaderT AppEnv IO a)
    deriving ( Functor, Applicative, Monad, MonadIO, MonadReader AppEnv
             , MonadUnliftIO, Generic )

runAppM :: AppM a -> AppEnv -> IO a
runAppM (AppM x) = runReaderT x

instance MonadResource AppM where
    focusResource l = view #resources >>= readMVar <&> (^. l)

    writeEvChan ev = do
        chan <- focusResource #eventChan
        liftIO $ B.writeBChan chan ev

    changeFocus name = view #resources
        >>= modify_ (return . (#focusRing %~ B.focusSetCurrent name))

    writeLogChan logl = do
        LogChan logChan <- focusResource #logChan
        atomically $ writeTBQueue logChan logl

    runLogWorker (LogChan logChan) = void . forkIO . forever $ do
        running <- focusResource #logWorkerRunning
        isEmptyMVar running >>= \case
            True  -> return ()
            False -> takeMVar running >> runLog
        threadDelay 1000000
      where
        runLog              = do
            shouldWriteLog <- focusSettings #shouldWriteLog
            work logChan shouldWriteLog

        work lc shouldWrite = do
            minLvl <- focusSettings #logLevel
            logLine@(_, _, lvl, _) <- atomically $ readTBQueue lc
            when (lvl >= minLvl) $ do
                let msg = formatLog logLine
                liftIO $ C.hPutStr stderr msg
                bool (return ()) (write msg) shouldWrite

        write msg = focusConfig #clean >>= \case
            True  -> return ()
            False -> do
                logFile <- focusConfig #logFile <&> toFilePath
                runResourceT . Q.appendFile logFile . Q.fromStrict $ msg

instance MonadUI AppM where
    handleEvent = \case
        AppEvent ae  -> case ae of
            WatchFs       -> void . forkIO $ cmpCached
            FsChange path -> reactFSChange path
            _             -> return ()
        VtyEvent evc -> do
            focus <- focusResource #focusRing <&> B.focusGetCurrent
            for_ focus $ \f -> do
                case f of
                    BrowserList   -> fmEvent evc
                    ConsoleEditor -> consoleEvent evc
                    _             -> return ()
        _            -> return ()

    runCommand cmd = view #echoed >>= swap_ Nothing >> run cmd
      where
        run = \case
            Control c    -> runControlCommand c
            Navigation n -> runNavCommand n
            FileOp f     -> runFileOpCommand f
            Toggle t     -> runSettingsComand t
            Visual v     -> runVisualCommand v

    showPopup pop = view #popup >>= swap_ (Just widget)
      where
        widget = B.txt (pop ^. #title)
            <=> B.hBorder
            <=> (pop ^. #item & (pop ^. #draw))

    hidePopup = view #popup >>= swap_ Nothing

    setBindingMode bm = view #bindingMode >>= swap_ bm

    getBindingMode = view #bindingMode >>= readMVar

    viewports = view #viewports

    extents = view #extents

    showBindingPrefix c = view #currentBindingPrefix
        >>= modify_ (return . (<> showEvChar c))

    hideBindingPrefix = view #currentBindingPrefix >>= swap_ mempty

    echo urgency message = view #echoed >>= swap_ (Just $ Echoed { .. })

instance MonadConsole AppM where
    getConsoleText g = g . B.getEditContents
        <$> (view #consoleEditor >>= readMVar)

    enterConsole im = changeFocus ConsoleEditor
        >> modifyConsole #inputMode (const $ Just im)

    doSearch searchText = do
        browserM <- currentBrowser
        for_ browserM $ \browser -> do
            icase <- focusSettings #iCaseSearch
            idx <- getCurrentIdx <&> fromMaybe 0
            let res = runSearch icase
                                searchText
                                (browser ^. #entries . B.listElementsL)
                                idx
            setBrowser
                $ maybe browser
                        (`selectChild` browser)
                        (res ^? #results . _head . #item . #path)
            leaveConsole
                >> saveInput #search searchText
                >> (view #searchResults >>= swap_ (Just res))
                >> loadNewPreview

    saveInput l x = modifyConsole (l . #history) (x <|)

    doCommand ctxt = saveInput #command ctxt
        >> either displayError runCommand (parseCommand ctxt)
        >> leaveConsole

    gotoResult direction = maybe (errMessage "No search results") processRes
        =<< currentSearchResults
      where
        processRes res =
            let results = res ^. #results
                idx     = res ^. #position
                newIdx  = scrollWrapped direction results idx
                entry   = results ^? ix newIdx
            in
                maybe (errMessage "No matching results")
                      (goto res newIdx)
                      entry

        goto res idx en = do
            browserM <- currentBrowser
            for_ browserM $ \browser -> view #searchResults
                >>= swap_ (Just $ res & #position .~ idx)
                >> setBrowser (selectChild (en ^. #item . #path) browser)
                >> loadNewPreview

    currentSearchResults = view #searchResults >>= readMVar

    setConsoleText g = view #consoleEditor
        >>= modify_ (\c -> return $! c & B.editContentsL %~ g)

    modifyConsole l g = view #console >>= modify_ (\c -> return $! c & l %~ g)

    currentConsole = view #console >>= readMVar

instance MonadNav AppM where
    setCwd path = view #cwd >>= swap_ path

    viewCwd = view #cwd >>= readMVar

    cd path@(AbsPath fp) = do
        access <- verifyAccess path
        bool writeMsg goto access
      where
        goto     = handle (displayError . FsError)
            $ traverse_ (uncurry savePosition) (recPositions path)
            >> D.setCurrentDirectory fp
            >> leaveCwd
            >> loadNewBrowser path

        writeMsg = errMessage $ "cd " <> T.pack fp <> ": access denied"

    verifyAccess (AbsPath fp) = liftIO $ PX.fileAccess fp True False False

    doesPathExist = D.doesPathExist

    isDir = D.doesDirectoryExist

    mkAbsPath wd fp = do
        absPath <- D.withCurrentDirectory wd
            . D.makeAbsolute
            . F.dropTrailingPathSeparator
            $ fp
        return $ AbsPath absPath

instance MonadBrowser AppM where
    type E AppM = Error

    listDir (AbsPath fp) = handle (return . Left . FsError)
        $ Right <$> (traverse (mkAbsPath fp) =<< D.listDirectory fp)

    getEntries path = either (const $ return mempty) mkEntries
        =<< listDir path
      where
        mkEntries fps = V.fromList . catMaybes <$> traverse mkEntry fps

    currentFm = view #fm >>= readMVar

    modifyFm l g = view #fm >>= modify_ (\fm -> return $! fm & l %~ g)

    findParent path = verifyAccess path >>= \case
        False -> return Nothing
        True  -> traverse mkParent (findParentPath path)
      where
        mkParent = newBrowser ParentList

    loadNewBrowser path = do
        isLoading <- view (#loading . #browser)
        tryTakeMVar isLoading >>= \case
            Just t  -> killThread t
            Nothing -> return ()
        modifyFm #browser (const Nothing)
        tid <- forkIO $ race_ showLoader newBr
        putMVar isLoading tid
      where
        newBr = do
            browser <- newBrowser BrowserList path
            savedPos <- lookupPosition path
            parent <- findParent path
            setCwd path
                >> setBrowser (maybe browser (`selectChild` browser) savedPos)
                >> setParent (selectChild path <$> parent)
                >> writeEvChan Loaded
                >> loadNewPreview

    yanked = view #yanked >>= readMVar

    marks = currentFm
        <&> (^.. #browser
             . _Just
             . #entries
             . traversed
             . filtered (^. #item . #marked))

    mkDirTreeImmediate (AbsPath fp) = do
        ps <- D.listDirectory fp
        tr <- for ps $ \p -> do
            let path = fp </> p
            D.doesDirectoryExist path >>= \case
                False -> return . Child $ fromFilePath path
                True  -> walkOneLevel path
        return . DirTree $ tr
      where
        walkOneLevel path = handleIO (\_ -> return $ Child mempty) $ do
            ps <- D.listDirectory path
            tr <- for ps $ \p -> return . Child . fromFilePath $ path </> p
            return . ChildTree . DirTree $ tr

instance MonadPreview AppM where
    mkPreview entry = tryIO (newPreview entry) >>= \case
        Right pr -> return pr
        Left _   -> return Unavailable

    readFile (AbsPath fp) = do
        maxLines <- focusSettings #maxPreviewLines
        b <- runResourceT
            . Q.toLazy_
            . Q.unlines
            . S.take maxLines
            . Q.lines
            . Q.readFile
            $ fp
        return $ either (const Nothing) (Just . LT.toStrict) (decodeUtf8' b)

    getFileInfo (AbsPath fp) = do
        output <- do
            file <- getHardDep "file"
            runProcessBlocking file [ "-Lb", fp ] mempty
        return $ maybe unknownFileInfo FInfo output
      where
        unknownFileInfo = FInfo $ T.pack fp <> ": unknown filetype"

    getPreview = view #fm >>= readMVar <&> (^. #preview)

    loadNewPreview = do
        isLoading <- view (#loading . #preview)
        tryTakeMVar isLoading >>= \case
            Just t  -> killThread t
            Nothing -> return ()
        modifyFm #preview (const Nothing)
        tid <- forkIO $ race_ showLoader setPr
        putMVar isLoading tid
      where
        setPr = do
            browserM <- currentBrowser
            for_ browserM $ \browser -> do
                !prev
                    <- traverse mkPreview (browser ^. #entries & currentEntry)
                modifyFm #preview (const prev)
                writeEvChan Loaded

instance MonadHistory AppM where
    readHistory = view #history >>= readMVar

    modifyHistory l g = view #history >>= modify_ (\h -> return $! h & l %~ g)

    focusHistory l = readHistory <&> (^. l)

    popRecent = do
        cwd <- viewCwd
        recent <- focusHistory #recent
        let (path, rest) = recentOrCurrentPath recent cwd
        loadNewBrowser path >> modifyHistory #recent (const rest)

    cacheBrowser path browser =
        modifyHistory #browserCache (cacheInsert path browser)

    uncacheBrowser path = modifyHistory #browserCache (cacheDelete path)

    getOwners (User uid)  = lookupOwners (liftIO . PX.getUserEntryForID)
                                         PX.userName
                                         #usernameCache
                                         uid
    getOwners (Group gid) = lookupOwners (liftIO . PX.getGroupEntryForID)
                                         PX.groupName
                                         #groupnameCache
                                         gid

    savePosition parent child =
        modifyHistory #savedPositions (at parent ?~ child)

    lookupPosition path = focusHistory $ #savedPositions . at path

instance MonadConfig AppM where
    toggleSettings l g = modifyConfig #settings (l %~ g)

    readConfig = view #config >>= readMVar

    focusConfig l = readConfig <&> (^. l)

    modifyConfig l g = view #config >>= modify_ (\c -> return $! c & l %~ g)

    bookmark k = do
        cwd <- viewCwd
        modifyConfig #bookmarks (at k ?~ mkBookmark k cwd)

    unBookmark k = modifyConfig #bookmarks (at k .~ Nothing)

    bookmarks = focusConfig #bookmarks

    readXdgData path = do
        xdgD <- xdgFile #dataDir path
        doesPathExist xdgD >>= \case
            False -> return Nothing
            True  -> Just . fromXdgData . T.decodeUtf8
                <$> (runResourceT . Q.toStrict_ . Q.readFile $ xdgD)

    writeXdgData path x = focusConfig #clean >>= \case
        True  -> return ()
        False -> do
            xdgD <- xdgFile #dataDir path
            void . runResourceT
                $ Q.writeFile xdgD (Q.fromStrict . T.encodeUtf8 $ toXdgData x)

    tags = focusConfig #tags

    toggleTag t entry = modifyConfig #tags (at (entry ^. #item . #path) .~ t)
        >> modifyEntries (B.listModify tagEntry)
      where
        tagEntry = #item . #tag .~ t

instance MonadErr AppM where
    catchIOErr_ g toErr = g `catchIO` (displayError . toErr)

    displayError e = echo Error (showErr e)

instance MonadFile AppM where
    open (Dir d) = d ^. #path & cd
    open (File f) = maybeM (message Nothing) run (getMimeType $ f ^. #path)
      where
        run mt = getExecutableForMimeType mt >>= \case
            Nothing  -> message $ Just mt
            Just exe -> do
                x <- lookupHardDep (exe ^. #name) <&> fromMaybe exe
                void . forkIO . runSh_
                    $ exeToCmd x (f ^. #path . to toFilePath . packed)

        message mt = errMessage $ "No executable found for: " <> ty
          where
            ty = maybe (f ^. #path . to mkExtension . position @1)
                       (^. position @1)
                       mt

    openWith o = o ^. #exe & lookupHardDep >>= \case
        Just exe -> void . forkIO . runSh_ $ exeToCmd exe arg
        Nothing  -> searchPath (o ^. #exe) >>= \case
            Nothing  -> errMessage
                $ mconcat [ "No executable ", o ^. #exe & quote, " found" ]
            Just exe -> void . forkIO . runSh_ $ exeToCmd exe arg
      where
        arg = o ^. #entry . #item . #path . #unAbsPath . packed

    copy putOpt = \case
        File f -> copyFile putOpt (f ^. #path)
        Dir d  -> copyDir putOpt (d ^. #path)

    move putOpt en = copy putOpt en >> delete en

    delete (File f) = f ^. #path & deleteFile
    delete (Dir d)  = d ^. #path & deleteDir

    touch name = touchOrCreate `catchIOErr_` FsError
      where
        touchOrCreate = doesPathExist name >>= \case
            True  -> liftIO $ PX.touchFile name
            False -> void . runResourceT $ Q.writeFile name ""

    mkdir name = D.createDirectory name `catchIOErr_` FsError

    yankPath y = view #yanked >>= modify_ (\ys -> return $! y : ys)

    clearYanked = view #yanked >>= swap_ []

    yankPathToClip (AbsPath fp) = focusConfig #clipboardSupport
        >>= maybe message (copyToClip $ Just fp)
      where
        copyToClip p clip = void . fork
            $ runProcess_ (clip ^. position @1 . #exePath)
                          (clip ^. position @1 . #defaultArgs)
                          p

        message           = errMessage "No clipboard executable found in $PATH"

    rename r = do
        cwd <- toFilePath <$> viewCwd
        newPath <- mkAbsPath cwd (r ^. #new . unpacked)
        doesPathExist (toFilePath newPath) >>= \case
            True  -> errMessage "Can't rename, path already exists"
            False -> do
                (D.renamePath `on` toFilePath) oldPath newPath
                    `catchIOErr_` FsError
                retag oldPath newPath
      where
        oldPath = r ^. #entry . #item . #path

instance MonadProcess AppM where
    runProcessBlocking exe args stdin =
        either (const $ return Nothing) (return . checkOutput)
        =<< tryIO process
      where
        process = P.readProcessWithExitCode (toFilePath exe)
                                            args
                                            (fromMaybe mempty stdin)

        checkOutput (exit, out, _) = checkProcessOutput exit out

    runProcess_ exe args stdin = process
      where
        process =
            void (P.readProcess (toFilePath exe) args (fromMaybe mempty stdin))
            `catchIOErr_` FsError

    runWithHandle c = do
        (si, so, se, process) <- P.createProcess_ mempty c
        case si of
            Just stdin -> return $ ProcHandle { stderr = se, stdout = so, .. }
            Nothing    -> do
                P.terminateProcess process
                throwIO $ MiscError "Process handle could not be acquired"

    runSh cmd = P.readCreateProcessWithExitCode c mempty <&> (^. _1)
      where
        c = cmd ^. #cmd . unpacked & P.shell

    runInTerm = liftIO . P.system . (^. #cmd . unpacked)

    runSh_ cmd = run
      where
        run        = if
            | needsTerm -> suspendAndRun (catchSh_ runInTerm cmd)
            | shouldFork -> void . forkIO $ catchSh_ runSh cmd
            | otherwise -> catchSh_ runSh cmd

        flags      = cmd ^. #flags

        shouldFork = Fork `elem` flags

        needsTerm  = NeedsTerm `elem` flags

    searchPath = fmap (fromFilePath <$>) <$> (D.findExecutable . T.unpack)

    suspendAndRun (AppM x) = writeEvChan $ SuspendAndResume x

    fork = forkIO

instance MonadLogger AppM where
    monadLoggerLog loc src lvl msg = do
        t <- getTime
        writeLogChan (loc, t <> " " <> src, lvl, ML.toLogStr msg)
      where
        getTime = liftIO TM.getCurrentTime <&> (T.pack . TM.iso8601Show)
